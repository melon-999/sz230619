package com.atguigu.mybatis.demo;

import com.atguigu.mybatis.bean.MyBean;

import java.util.Comparator;

/**
 * Created by Smexy on 2023/10/24
 *
介绍:
 lamda表达式:
     用函数式编程的思想帮你为函数式接口创建实例。 f(x) -> x+1

 函数式接口：
    接口上标准了@Functional注解，称为函数式接口。
    特征:
        除了从Object中继承的方法外，只有一个需要实现的抽象方法！

 lamda表达式，就是对函数式接口中，仅有的需要实现的抽象方法的实现！
 --------------------------
语法:
    (参数列表) -> {方法体}
        1.参数列表中的参数类型是可以省略，由编译器去推导
        2.如果参数列表中只有一个参数，那么()可以省略
        3.方法体中如果只有一行代码，那么{}可以省略
          如果这一行代码是return，return可以省略
 -----------------------------
 为一个函数式接口提供实例有哪些方式?
    举例: 接口为A
    1.编写一个外部类，实现接口。
         B implements A
          接口的实现，需要在多个类中使用！
            class C {
            main(){
                new B().xxx()
            }
        }

             class D {
             main(){
             new B().xxx()
             }
             }

    2.编写一个内部类，实现接口
        B 仅仅在当前类中使用！
         class E {
             main(){
             new B().xxx()
             }
            class B implements A{}
         }

    3.编写一个匿名内部类，实现接口
        B 仅仅在当前类中使用，仅仅使用一次！省去B的签名
         class F {
         main(){
           new B(){}.xxx()
         }

         }
    4.编写lamda表达式
        作用和匿名内部类一模一样。表现形式上，会使用一个函数来表示你要调用的接口的抽象方法的实现。
         class G {
         main(){
            ()->{}.xxx()
         }


 */
public class LamdaDemo1
{
    public static void main(String[] args) {

        //匿名内部类
        Comparator c1= new Comparator<Integer>(){
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1.compareTo(o2);
            }
        };

        System.out.println(c1.compare(1, 2));

        //lamda表达式实现
        Comparator<Integer> c2 = (Integer a,Integer b) -> {
            int result = a.compareTo(b);
            return result;
        };

        System.out.println(c2.compare(1, 2));

        //简化lamda
        //lamda表达式实现  c3是一个数学函数
        Comparator<Integer> c3 = (a,b) -> a.compareTo(b);
        System.out.println(c3.compare(1, 2));

        /*
            lamda表达式的特例: 方法引用。
                如果函数的实现和某一个方法的实现一模一样，那么本着不要重复造轮子，
                要站在巨人的肩膀上的原则，可以直接告诉编译器，这个函数的实现请参考某个类现成的方法。

             方法引用的格式:
                    引用的是静态方法:   类名::方法名
                    引用的是实例方法:   对象::方法名
         */
        Comparator<Integer> c4 = (Integer x,Integer y) -> {
            return (x < y) ? -1 : ((x == y) ? 0 : 1);
        };
        System.out.println(c4.compare(1, 2));

        Comparator<Integer> c5 = MyBean::haha;
        System.out.println(c5.compare(1, 2));


    }
}

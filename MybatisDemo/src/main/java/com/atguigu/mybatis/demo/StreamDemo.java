package com.atguigu.mybatis.demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Smexy on 2023/10/24
 */
public class StreamDemo
{
    public static void main(String[] args) {

        List<Integer> data = Arrays.asList(1, 2, 3, 4, 5);

        //List<Integer> result = new ArrayList<>();
        //把集合中的每个数+10,再取偶数
        // 少用。效率低。串行处理
       /* for (Integer num : data) {
            if ((num + 10) % 2 == 0){
                result.add(num + 10);
            }
        }*/

        //牢记： 只要是处理集合都用Stream
        Stream<Integer> stream = data.stream();

        /*List<Integer> result = stream
            .map(new Function<Integer, Integer>()
            {
                @Override
                public Integer apply(Integer element) {
                    return element + 10;
                }
            })
            .filter(new Predicate<Integer>()
            {
                //只留下返回true的element
                @Override
                public boolean test(Integer element) {
                    return element % 2 == 0;
                }
            })
            .collect(Collectors.toList());*/

        List<Integer> result = stream
            .map(num ->  num + 10)
            .filter(num -> num % 2 == 0)
            .collect(Collectors.toList());

        System.out.println(result);


    }
}

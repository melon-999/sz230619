package com.atguigu.mybatis.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Smexy on 2023/10/23
 *  一个标准的JavaBean需要有以下特征:
 *      1.空参构造器
 *      2.为私有属性提供公共的getter,setter
 */
@NoArgsConstructor //为类提供空参构造器
@AllArgsConstructor //为类提供全参构造器
@Data  //为为私有属性提供公共的getter,setter，重写hashcode，toString()
public class Employee
{
    private Integer id;
    private String lastName;
    private String gender;
    private String email;

    public static void main(String[] args) {
        Employee employee = new Employee();
        System.out.println(new Employee(1, "tom", "a", "b"));


    }

   /* public void setLast_name(String a){
        this.lastName = a;
    }

    public void setB(String a){
        this.lastName = a;
    }*/
}

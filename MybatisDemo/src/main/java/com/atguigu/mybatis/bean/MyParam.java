package com.atguigu.mybatis.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Smexy on 2023/10/23
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MyParam
{
    private Integer param1;
    private String  param2;
}

package com.atguigu.mybatis.bean;

/**
 * Created by Smexy on 2023/10/24
 */
public class MyBean
{
    public static int haha(Integer x, Integer y) {
        return (x < y) ? -1 : ((x == y) ? 0 : 1);
    }
}

package com.atguigu.mybatis.mapper;

import com.atguigu.mybatis.bean.Employee;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by Smexy on 2023/10/23



 */
public interface EmployeeAnnotationMapper
{
    @Select("select * from employee where id = #{xxx}")
    Employee getEmployeeById(Integer id);

    @Delete("delete from employee where id = #{xxx}")
    void deleteEmployeeById(Integer id);

}

package com.atguigu.mybatis.mapper;

import com.atguigu.mybatis.bean.Employee;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by Smexy on 2023/10/23
 *

 在sql中如果要编写占位符，格式是 #{xxx}。
        #{xxx} 会根据参数的类型，自动拼接格式。
            例如参数是字符串类型，会自动拼接''.
            参数位置的占位符，都用#{},可以防范sql注入风险。

    也可以使用${xxx}
            ${xxx}将参数直接拼接上去，不做任何处理！
            表名位置，使用${}
            存在sql注入风险，不能在参数位置使用${}

 */
public interface ParamStyleExampleMapper
{
    @Select("select * from ${t} where last_name = #{name}")
    Employee getEmployeeByName(@Param("name") String name,@Param("t") String table);


    @Select("select * from employee where gender = #{gender}")
    List<Employee> getEmpsByCondition(@Param("gender") String gender);



}

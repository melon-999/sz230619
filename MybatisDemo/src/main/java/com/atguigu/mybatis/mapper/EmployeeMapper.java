package com.atguigu.mybatis.mapper;

import com.atguigu.mybatis.bean.Employee;

import java.util.List;

/**
 * Created by Smexy on 2023/10/23

 Java都是面向接口编程。
    以后只要是实现一个功能。只要这个功能不是一次性的！需要在以后反复维护，迭代的，都是先声明一个接口。

 套路:
        先写接口，再提供实现类。
 但是:
        在Mybatis中,如果编写的是Dao(Mapper)，无需提供实现类。
        Mybatis提供了动态代理技术，帮你去为接口自动创建一个实现类，返回实例。

 */
public interface EmployeeMapper
{
    Employee getEmployeeById(Integer id);

    List<Employee> getAll();

    //增删改可以不要返回值
    void insertEmployee(Employee e);

    void updateEmployee(Employee e);

    void deleteEmployeeById(Integer id);


}

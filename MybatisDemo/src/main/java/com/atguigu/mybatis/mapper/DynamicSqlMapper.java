package com.atguigu.mybatis.mapper;

import com.atguigu.mybatis.bean.Employee;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Smexy on 2023/10/23
 */
public interface DynamicSqlMapper
{
    List<Employee> getEmpsByCondition(@Param("gender") String gender, @Param("id") Integer id, @Param("name") String name);

}

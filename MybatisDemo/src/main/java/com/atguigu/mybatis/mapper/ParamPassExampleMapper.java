package com.atguigu.mybatis.mapper;

import com.atguigu.mybatis.bean.Emp;
import com.atguigu.mybatis.bean.Employee;
import com.atguigu.mybatis.bean.MyParam;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * Created by Smexy on 2023/10/23

    如何将方法中的参数，传递给sql中的占位符。
    如果sql中需要传入多个占位符，那么方法的参数列表该如何设计? 及 #{xxx}中的xxx如何编写？

 */
public interface ParamPassExampleMapper
{

    /*
        sql中需要传入的占位符个数比较少，可以直接平铺开。
        #{xxx}中的xxx如何编写?
            需要使用@Param注解，为每个参数的xxx起名字。
     */
    @Select("select * from employee where id > #{a} and gender = #{b}")
    List<Employee> query1(@Param("a") Integer id, @Param("b") String gender);


    /*
           sql中需要传入的占位符个数比较多，可以使用Bean来封装参数。
           #{xxx}中的xxx如何编写?
               如果参数是一个Bean，使用#{Bean的属性名}获取Bean对应的属性值！
        */
    @Select("select * from employee where id > #{param1} and gender = #{param2}")
    List<Employee> query2(MyParam mp);


    /*
           sql中需要传入的占位符个数比较多，不想再去写一个Bean，可以使用Map来封装参数。
           #{xxx}中的xxx如何编写?
               如果参数是一个Map，使用#{map中的key}获取对应的value！
        */
    @Select("select * from employee where id > #{a} and gender = #{c}")
    List<Employee> query3(Map<String,Object> map);

}

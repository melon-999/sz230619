package com.atguigu.mybatis.mapper;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Smexy on 2023/10/23
 */
public class EmpMapperTest
{
    private SqlSessionFactory sqlSessionFactory;

    {
        String resource = "mybatis.xml";   //定义mybatis的配置
        InputStream inputStream = null;
        try {
            inputStream = Resources.getResourceAsStream(resource);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    }

    @Test
    public void getAllEmp() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        try {
            EmpMapper mapper = sqlSession.getMapper(EmpMapper.class);
            //使用Mapper，进行CRUD
            System.out.println(mapper.getAll());
        }finally {
            sqlSession.close();
        }
    }


}
package com.atguigu.mybatis.mapper;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.*;

/**
 * Created by Smexy on 2023/10/23
 */
public class DynamicSqlMapperTest
{

    private SqlSessionFactory sqlSessionFactory;

    {
        String resource = "mybatis.xml";   //定义mybatis的配置
        InputStream inputStream = null;
        try {
            inputStream = Resources.getResourceAsStream(resource);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    }



    @Test
    public void getEmpsByCondition() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        try {
            DynamicSqlMapper mapper = sqlSession.getMapper(DynamicSqlMapper.class);
            //调用方法的时候，传入的参数个数是不确定的！ 希望sql可以根据传入参数的情况，动态变化
            System.out.println(mapper.getEmpsByCondition(null,1,null));
            //System.out.println(mapper.getEmpsByCondition("male",1,null));
            //System.out.println(mapper.getEmpsByCondition("male",1,"jack"));
        }finally {
            sqlSession.close();
        }
    }
}
package com.atguigu.mybatis.mapper;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.*;

/**
 * Created by Smexy on 2023/10/23
 */
public class EmployeeAnnotationMapperTest
{
    private SqlSessionFactory sqlSessionFactory;

    {
        String resource = "mybatis.xml";   //定义mybatis的配置
        InputStream inputStream = null;
        try {
            inputStream = Resources.getResourceAsStream(resource);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    }

    @Test
    public void getEmployeeById() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        try {
            EmployeeAnnotationMapper mapper = sqlSession.getMapper(EmployeeAnnotationMapper.class);
            //使用Mapper，进行CRUD
            System.out.println(mapper.getEmployeeById(1));
        }finally {
            sqlSession.close();
        }
    }

    @Test
    public void deleteEmployeeById() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        try {
            EmployeeAnnotationMapper mapper = sqlSession.getMapper(EmployeeAnnotationMapper.class);
            //使用Mapper，进行CRUD
            mapper.deleteEmployeeById(4);
        }finally {
            sqlSession.close();
        }
    }
}
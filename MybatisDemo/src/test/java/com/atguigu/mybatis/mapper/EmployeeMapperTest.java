package com.atguigu.mybatis.mapper;

import com.atguigu.mybatis.bean.Employee;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * Created by Smexy on 2023/10/23
 */
public class EmployeeMapperTest
{
    private SqlSessionFactory sqlSessionFactory;

    {
        String resource = "mybatis.xml";   //定义mybatis的配置
        InputStream inputStream = null;
        try {
            inputStream = Resources.getResourceAsStream(resource);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    }
    //一旦一个变量作为属性，在多个方法中复用，必须保证线程安全！
    //Mybatis官方声明了SqlSession不是线程安全的，因此不能作为属性(实例的属性，静态的属性)，而是每一个方法都有自己的SqlSession

    //SqlSession sqlSession ;  错误的示范
    @Test
    public void template() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            //用Mybatis提供的动态代理技术，获取一个Mapper接口的实例
            //完整签名:  com.sun.proxy.$Proxy5 implements com.atguigu.mybatis.mapper.EmployeeMapper
            EmployeeMapper mapper = sqlSession.getMapper(EmployeeMapper.class);
            System.out.println(mapper.getClass().getName()); //com.sun.proxy.$Proxy5
            //打印实现的所有的接口
            System.out.println(Arrays.toString(mapper.getClass().getInterfaces())); //[interface com.atguigu.mybatis.mapper.EmployeeMapper]
        }finally {
            sqlSession.close();
        }
    }

    @Test
    public void getEmployeeById() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            EmployeeMapper mapper = sqlSession.getMapper(EmployeeMapper.class);
            System.out.println(mapper.getEmployeeById(2));
        }finally {
            sqlSession.close();
        }
    }

    @Test
    public void getAll() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            EmployeeMapper mapper = sqlSession.getMapper(EmployeeMapper.class);
            System.out.println(mapper.getAll());
        }finally {
            sqlSession.close();
        }
    }

    @Test
    public void insertEmployee() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        try {
            EmployeeMapper mapper = sqlSession.getMapper(EmployeeMapper.class);
            Employee employee = new Employee(null, "jackma", "male", "jackma@ali.com");
            mapper.insertEmployee(employee);
        }finally {
            sqlSession.close();
        }
    }

    @Test
    public void updateEmployee() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        try {
            EmployeeMapper mapper = sqlSession.getMapper(EmployeeMapper.class);
            Employee e = mapper.getEmployeeById(4);
            e.setLastName("ponyma");
            mapper.updateEmployee(e);
        }finally {
            sqlSession.close();
        }
    }

    /*
            读: select
            写： update,insert,delete
                    一定提交事务！
                    原生JDBC是自动提交事务，但是mybatis需要配置！
     */
    @Test
    public void deleteEmployeeById() {
        //让这个连接自动提交事务
        //SqlSession sqlSession = sqlSessionFactory.openSession(true);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            EmployeeMapper mapper = sqlSession.getMapper(EmployeeMapper.class);
            mapper.deleteEmployeeById(3);
            //手动提交
            sqlSession.commit();
        }finally {
            sqlSession.close();
        }
    }
}
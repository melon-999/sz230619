package com.atguigu.mybatis.mapper;

import com.atguigu.mybatis.bean.MyParam;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import static org.junit.Assert.*;

/**
 * Created by Smexy on 2023/10/23
 */
public class ParamPassExampleMapperTest
{
    private SqlSessionFactory sqlSessionFactory;

    {
        String resource = "mybatis.xml";   //定义mybatis的配置
        InputStream inputStream = null;
        try {
            inputStream = Resources.getResourceAsStream(resource);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    }


    @Test
    public void query1() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        try {
            ParamPassExampleMapper mapper = sqlSession.getMapper(ParamPassExampleMapper.class);
            //使用Mapper，进行CRUD
            System.out.println(mapper.query1(1,"male"));
        }finally {
            sqlSession.close();
        }
    }

    @Test
    public void query2() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        try {
            ParamPassExampleMapper mapper = sqlSession.getMapper(ParamPassExampleMapper.class);
            //使用Mapper，进行CRUD
            System.out.println(mapper.query2(new MyParam(0,"male")));
        }finally {
            sqlSession.close();
        }
    }

    @Test
    public void query3() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        try {
            ParamPassExampleMapper mapper = sqlSession.getMapper(ParamPassExampleMapper.class);
            HashMap<String, Object> map = new HashMap<>();
            map.put("a",0);
            map.put("c","male");
            //使用Mapper，进行CRUD
            System.out.println(mapper.query3(map));
        }finally {
            sqlSession.close();
        }
    }
}
package com.atguigu.mybatis.mapper;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.*;

/**
 * Created by Smexy on 2023/10/23
 */
public class ParamStyleExampleMapperTest
{
    private SqlSessionFactory sqlSessionFactory;

    {
        String resource = "mybatis.xml";   //定义mybatis的配置
        InputStream inputStream = null;
        try {
            inputStream = Resources.getResourceAsStream(resource);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    }

    @Test
    public void getEmployeeByName() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        try {
            ParamStyleExampleMapper mapper = sqlSession.getMapper(ParamStyleExampleMapper.class);
            //使用Mapper，进行CRUD
            System.out.println(mapper.getEmployeeByName("tom","employee"));
        }finally {
            sqlSession.close();
        }
    }

    @Test
    public void testSqlInjection() {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        try {
            ParamStyleExampleMapper mapper = sqlSession.getMapper(ParamStyleExampleMapper.class);
            //使用Mapper，进行CRUD
            System.out.println(mapper.getEmpsByCondition("'female' or id > 0"));
        }finally {
            sqlSession.close();
        }
    }
}
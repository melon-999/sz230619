package com.atguigu.flink.window;

import com.atguigu.flink.function.WaterSensorMapFunction;
import com.atguigu.flink.pojo.WaterSensor;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * Created by Smexy on 2023/11/14

 */
public class Demo6_Reduce
{
    public static void main(String[] args) {

        Configuration conf = new Configuration();
        conf.setInteger("rest.port", 3333);
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);

                env
                   .socketTextStream("hadoop102", 8888)
                   .map(new WaterSensorMapFunction())
                    .countWindowAll(5l)
                   //默认帮你keyBy
                    .reduce(new ReduceFunction<WaterSensor>()
                    {
                        @Override
                        public WaterSensor reduce(WaterSensor value1, WaterSensor value2) throws Exception {
                            System.out.println("Demo6_Reduce.reduce");
                            //累加vc
                            value1.setVc(value1.getVc() + value2.getVc());
                            return value1;
                        }
                    })
                    .map(w -> w.getVc())
                    .print();


                try {
                            env.execute();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

    }
}

package com.atguigu.flink.utils;

import org.apache.flink.streaming.api.windowing.windows.TimeWindow;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Created by Smexy on 2023/11/13
 */
public class MyUtil
{
    //将窗口中的Iterable<T> elements封装为List<T>
    public static <T> List<T> parseToList(Iterable<T> elements){
        List<T> result = StreamSupport.stream(elements.spliterator(), true)
                                       .collect(Collectors.toList());

        return result;
    }

    //声明时间格式
    private static DateTimeFormatter dateTime = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    //获取时间窗口的时间范围，由long的时间戳转换为时间格式
    public static String parseTimeWindow(TimeWindow w){

        String startTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(w.getStart()), ZoneId.of("Asia/Shanghai")).format(dateTime);
        String endTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(w.getEnd()), ZoneId.of("Asia/Shanghai")).format(dateTime);

        return "[" + startTime + "," + endTime +")";
    }
}

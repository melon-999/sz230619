package com.atguigu.flink.function;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;

import java.util.Arrays;

/**
 * Created by Smexy on 2023/11/8
 */
public class MyWordCountFlatmapFunction implements FlatMapFunction<String, Tuple2<String,Integer>>
{
    @Override
    public void flatMap(String value, Collector<Tuple2<String, Integer>> out) throws Exception {
        String[] words = value.split(" ");
        Arrays.stream(words)
              .forEach(
                  word -> out.collect(Tuple2.of(word,1))
              );
    }
}

package com.atguigu.flink.function;

import com.atguigu.flink.pojo.WaterSensor;
import org.apache.flink.api.common.functions.MapFunction;

/**
 * Created by Smexy on 2023/11/11
 */
public class WaterSensorMapFunction implements MapFunction<String, WaterSensor>
{
    @Override
    public WaterSensor map(String value) throws Exception {
        String[] words = value.split(",");
        return new WaterSensor(
            words[0],
            Long.valueOf(words[1]),
            Integer.valueOf(words[2])
        );
    }
}

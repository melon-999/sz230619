package com.atguigu.flink.datastreamapi.source;

import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Smexy on 2023/11/11

 用于测试业务逻辑是否正确
 */
public class Demo1_SourceFromCollection
{
    public static void main(String[] args) {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setParallelism(1);

        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6);

        //从集合中构造一个流,必须是Collection类型的集合
        DataStreamSource<Integer> source = env.fromCollection(list);

        source.print();

        //基于元素构造
        DataStreamSource<Integer> source2 = env.fromElements(1, 2, 3, 4, 5, 6);
        source2.printToErr();

        try {
            env.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}

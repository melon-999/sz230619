package com.atguigu.flink.datastreamapi;

import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * Created by Smexy on 2023/11/10
 */
public class Demo2_JobGraphExec
{
    public static void main(String[] args) throws Exception {

        Configuration conf = new Configuration();
        conf.setInteger("rest.port", 3333);
        conf.setString("taskmanager.memory.network.min","1gb");
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);

        //env.setParallelism(2);

        env.socketTextStream("hadoop102",8888).name("s")
           .map(x -> x).name("m1")
           .map(x -> x).name("m2").setParallelism(2)
           .map(x -> x).name("m3")
           .map(x -> x).name("m4")
           .global()
           .map(x -> x).name("m5")
           .map(x -> x).name("m6")
           .keyBy(x -> x)
           .map(x -> x).name("m7")
           .map(x -> x).name("m8").setParallelism(3)
           .map(x -> x).name("m9")
           .shuffle()
           .map(x -> x).name("m10")
           .map(x -> x).name("m11")
           .print().name("p").setParallelism(1);

        env.execute();

    }
}

package com.atguigu.flink.datastreamapi.agg;

import com.atguigu.flink.function.WaterSensorMapFunction;
import com.atguigu.flink.pojo.WaterSensor;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * Created by Smexy on 2023/11/11


 无keyBy不聚合。
        sum,min,max: 输出的数据，除了聚合列，其他列都采取当前分组第一条数据的值。

 */
public class Demo1_Agg
{
    public static void main(String[] args) {
        
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        //聚合vc
        SingleOutputStreamOperator<WaterSensor> ds = env
            .socketTextStream("hadoop102", 8888)
            .map(new WaterSensorMapFunction());

        //按照key分组后的流
        KeyedStream<WaterSensor, String> ds2 = ds.keyBy(WaterSensor::getId);

        //常见的聚合方法，都是定义在KeyedStream类型中
        ds2
            //.sum("vc")
            //.min("vc")
            .max("vc")
            .print();


        try {
                            env.execute();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
        
    }
}

package com.atguigu.flink.datastreamapi.commonapi;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * Created by Smexy on 2023/11/11
 */
public class Demo3_Filter
{
    public static void main(String[] args) {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setParallelism(1);

        env
            .fromElements(1,2,3,4,5,6)
            //留下 表达式返回true的元素
            .filter(x -> x % 2 == 0)
            .print();

        try {
            env.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}

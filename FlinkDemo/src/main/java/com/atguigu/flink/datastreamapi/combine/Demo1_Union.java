package com.atguigu.flink.datastreamapi.combine;

import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * Created by Smexy on 2023/11/13
 */
public class Demo1_Union
{
    public static void main(String[] args) {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setParallelism(1);


        DataStreamSource<Integer> ds1 = env.fromElements(1, 2, 3, 4, 5);
        DataStreamSource<Integer> ds2 = env.fromElements(11, 12, 13, 14, 15);
        DataStreamSource<Integer> ds3 = env.fromElements(21, 22, 23, 24, 25);
        DataStreamSource<String> ds4 = env.fromElements("33");

        DataStream<Integer> ds5 = ds1.union(ds2, ds3);

        ds4.print();

        try {
            env.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}

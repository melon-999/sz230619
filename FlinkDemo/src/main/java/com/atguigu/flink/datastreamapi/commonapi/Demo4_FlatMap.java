package com.atguigu.flink.datastreamapi.commonapi;

import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

/**
 * Created by Smexy on 2023/11/11
 *

 完全可以替代filter + map
 */
public class Demo4_FlatMap
{
    public static void main(String[] args) {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setParallelism(1);

        //留下偶数，且将留下的数+10，复制2份输出到下游
        env
            .fromElements(1,2,3,4,5,6)
            .flatMap((Integer x, Collector<Integer> out) -> {
                //是偶数
                if (x % 2 == 0){
                    out.collect(x + 10);
                    out.collect(x + 10);
                }
            }).returns(Types.INT)
            .print();

        try {
            env.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}

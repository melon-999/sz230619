package com.atguigu.flink.datastreamapi.source;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * Created by Smexy on 2023/11/11
 */
public class Demo2_SourceFromNetPort
{
    public static void main(String[] args) {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setParallelism(1);

        /*
            非并行运行的算子
                这个api是老式构造source的api
                    addSource(SocketTextStreamFunction implements SourceFunction<String>)

                只要addSource(SourceFunction x)，并行度必须强制为1。
         */
        env
            .socketTextStream("hadoop102",8888)
            .print();


        try {
            env.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}

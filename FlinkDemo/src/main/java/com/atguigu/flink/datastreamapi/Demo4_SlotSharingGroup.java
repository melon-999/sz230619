package com.atguigu.flink.datastreamapi;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * Created by Smexy on 2023/11/10
 */
public class Demo4_SlotSharingGroup
{
    // http://hadoop103:37657   仅仅适用于session模式
    public static void main(String[] args) {

        //StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        // LocalStreamEnvironment : 只在idea机器上跑
        //System.out.println(env.getClass().getName());
        StreamExecutionEnvironment env = StreamExecutionEnvironment.createRemoteEnvironment(
            "hadoop103",
            37657,
            "jars/FlinkDemo-1.0-SNAPSHOT.jar"
        );

        env.setParallelism(1);

        env
            .socketTextStream("hadoop102", 8888) // default
            .map(x -> x).name("m1").setParallelism(3).slotSharingGroup("a")  // a
            .map(x -> x).name("m2")                   // a
            .map(x -> x).name("m3").setParallelism(4)  // a
            .map(x -> x).name("m4").slotSharingGroup("b")                   // b
            .print().name("p").setParallelism(2);    // b



        try {
            env.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}

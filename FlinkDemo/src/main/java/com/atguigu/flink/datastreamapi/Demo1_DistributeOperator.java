package com.atguigu.flink.datastreamapi;

import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * Created by Smexy on 2023/11/10
 */
public class Demo1_DistributeOperator
{
    public static void main(String[] args) {

     Configuration conf = new Configuration();
     conf.setInteger("rest.port", 3333);
     StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);

     env.setParallelism(3);
     env.disableOperatorChaining();

        env
            .socketTextStream("hadoop102", 8888)
            //.rebalance()//负载均衡
            //.rescale() //负载均衡
            //.shuffle()  //随机
            //.global() // 全局汇总,下游的并行度可以设置为1
            //.keyBy(s -> s) //按照key进行分发。同一种key的数据发到下游的同一个Task，不同key的数据也可能发到下游的同一个Task
            //.broadcast() //广播，用于广播配置
            //.forward() //前提是上下游的并行度相等。 forward是算子链的必要条件
            .print().name("myprint").setParallelism(2);


       try {
           env.execute();
       } catch (Exception e) {
           e.printStackTrace();
       }



    }
}

package com.atguigu.flink.state.keyedstate;

import com.atguigu.flink.function.WaterSensorMapFunction;
import com.atguigu.flink.pojo.WaterSensor;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.RuntimeContext;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.TimerService;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;

/**
 * Created by Smexy on 2023/11/15
 *

 需要保存的数据(状态)有:
    基于事件时间
   1.定时器的时间
   2.记录上一个传感器的水位
   3.提供一个标记，来控制定时器的创建。只有当前没有定时器时，才创建


    操作时，只使用一种key
 */
public class Demo6_TimerExec
{
    public static void main(String[] args) {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setParallelism(1);

        WatermarkStrategy<WaterSensor> watermarkStrategy = WatermarkStrategy
            .<WaterSensor>forMonotonousTimestamps()
            .withTimestampAssigner( (e, ts) -> e.getTs());

        env
            .socketTextStream("hadoop102", 8888)
            .map(new WaterSensorMapFunction())
            .assignTimestampsAndWatermarks(watermarkStrategy)
            .keyBy(WaterSensor::getId)
            .process(new KeyedProcessFunction<String, WaterSensor, String>()
            {

                private String a = "hahaha";
                //编程时，只写了一个属性，flink会为每个key都创建一套属性
                private ValueState<Long> time;
                private ValueState<Integer> lastVc;
                private ValueState<Boolean> ifNeedTimer;

                @Override
                public void open(Configuration parameters) throws Exception {
                    //获取状态，不能操作状态
                    RuntimeContext runtimeContext = getRuntimeContext();
                    time = runtimeContext.getState(new ValueStateDescriptor<>("time", Long.class));
                    lastVc = runtimeContext.getState(new ValueStateDescriptor<>("lastVc", Integer.class));
                    ifNeedTimer = runtimeContext.getState(new ValueStateDescriptor<>("ifNeedTimer", Boolean.class));

                }

                @Override
                public void processElement(WaterSensor value, Context ctx, Collector<String> out) throws Exception {
                    System.out.println(a);
                    String key = ctx.getCurrentKey();
                    TimerService timerService = ctx.timerService();

                    Boolean ifNeedTimerValue = ifNeedTimer.value();

                    if (ifNeedTimerValue == null || ifNeedTimerValue){
                        //创建定时器
                        time.update(ctx.timestamp() + 5000);
                        timerService.registerEventTimeTimer(time.value());
                        System.out.println(key +"指定了以下时间的定时器:"+time.value());
                        ifNeedTimer.update(false);
                        //记录当前传感器的水位
                        lastVc.update(value.getVc());
                    }else if (value.getVc() < lastVc.value()){
                        //水位下降
                        timerService.deleteEventTimeTimer(time.value());
                        System.out.println(key +"删除了以下时间的定时器:"+time.value());
                        //一切重置归0
                        ifNeedTimer.update(true);
                        lastVc.update(0);
                    }else {
                        //记录当前传感器的水位
                        lastVc.update(value.getVc());

                    }

                }

                //定时器到点执行的程序
                @Override
                public void onTimer(long timestamp,OnTimerContext ctx, Collector<String> out) throws Exception {
                    out.collect(ctx.getCurrentKey() +" 到了"+timestamp+"点,5s内水位连续上升,预警......");
                    //一切重置归0
                    ifNeedTimer.update(true);
                    lastVc.update(0);
                }



            })
            .print();


        try {
            env.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

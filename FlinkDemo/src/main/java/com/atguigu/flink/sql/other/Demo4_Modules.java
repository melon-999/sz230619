package com.atguigu.flink.sql.other;

import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.TableEnvironment;
import org.apache.flink.table.module.hive.HiveModule;
//import org.apache.flink.table.module.hive.HiveModule;

import java.util.Arrays;

/*
    modules: 管理flink中的函数模块。
 */

public class Demo4_Modules
{
    public static void main(String[] args) {

        EnvironmentSettings environmentSettings = EnvironmentSettings.newInstance().inStreamingMode().build();
        TableEnvironment tableEnvironment = TableEnvironment.create(environmentSettings);

        //1.创建hive的函数模块
        HiveModule hiveModule = new HiveModule();
        //2.注册
        tableEnvironment.loadModule("hive",hiveModule);
        //3.加载  hive和core中有同名的函数，默认使用时参考加载的顺序
        tableEnvironment.useModules("core","hive");
        /*
            默认加载[core]模块
            core模块内置了flink中的内置函数。

            flink在开发时，考虑到了兼容前辈(hive)。flink的UDF函数的定义和Hive一模一样。
         */
        System.out.println(Arrays.toString(tableEnvironment.listModules()));


        tableEnvironment.sqlQuery("select split('a,b,c',',')").execute().print();

    }
}

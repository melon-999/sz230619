package com.atguigu.flink.sql.other;

import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.TableEnvironment;

/**
 * Created by Smexy on 2023/7/25
 *
 *  sqlhint： 查询小提示。用于临时修改表中的元数据，只在当前查询有效。

 */
// 在需要给出提示的地方使用: /*+ OPTIONS('k'='v') */
public class Demo6_SqlHint
{
    public static void main(String[] args) {

        EnvironmentSettings environmentSettings = EnvironmentSettings.newInstance().inBatchMode().build();
        TableEnvironment env = TableEnvironment.create(environmentSettings);

        String createTableSql = " create table t1 ( id STRING, ts BIGINT, vc INT  " +
            "  ) with (" +
            " 'connector' = 'filesystem', " +
            " 'path' = 'data/ws4.json', " +
            " 'format' = 'json'  " +
            ")";

        env.executeSql(createTableSql);

        env.sqlQuery("select * from t1  /*+ OPTIONS('path'='data/ws.json') */ ")
           .execute().print();

    }
}

package com.atguigu.flink.sql.connector;

import com.atguigu.flink.function.WaterSensorMapFunction;
import com.atguigu.flink.pojo.WaterSensor;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

/**



 */
public class Demo8_WriteUpsertKafka2
{
    public static void main(String[] args) {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(2);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        SingleOutputStreamOperator<WaterSensor> ds = env
            .socketTextStream("hadoop102", 8888)
            .map(new WaterSensorMapFunction());

        //源表
        Table table = tableEnv.fromDataStream(ds);
        tableEnv.createTemporaryView("t2",table);

        //group by 如果有多个字段，必须是写出表的联合主键
        String createTableSql = "CREATE TABLE t1 (" +
            "  id STRING  ," +
            "  ts BIGINT  ," +
            "  vc INT ," +
            "   PRIMARY KEY (id,ts) NOT ENFORCED  " +
            ")  WITH (" +
            " 'connector' = 'upsert-kafka'," +
            "  'topic' = 't6'," +
            "  'properties.bootstrap.servers' = 'hadoop102:9092'," +
            "  'key.format' = 'json' ," +
            "  'value.format' = 'json' " +
            ")";

        tableEnv.executeSql(createTableSql);

        //group by后的字段必须整体作为主键
       tableEnv.executeSql("insert into t1 select id,ts ,sum(vc) sumVc from t2 group by id,ts");

    }
}

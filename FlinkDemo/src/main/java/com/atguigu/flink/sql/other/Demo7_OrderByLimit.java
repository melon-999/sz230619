package com.atguigu.flink.sql.other;

import com.atguigu.flink.function.WaterSensorMapFunction;
import com.atguigu.flink.pojo.WaterSensor;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.TableEnvironment;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

/**
 * Created by Smexy on 2023/7/25
 *
 *      limit: 只支持BatchMode
 *      order by： 流模式下，第一个字段必须是时间,时间只能按照升序。
 */
public class Demo7_OrderByLimit
{
    public static void main(String[] args) {

        EnvironmentSettings environmentSettings = EnvironmentSettings.newInstance().inStreamingMode().build();
        TableEnvironment env = TableEnvironment.create(environmentSettings);

        String createTableSql = " create table t1 ( id STRING, ts BIGINT, vc INT , " +
            "  pt as PROCTIME() ," +
            "  et as TO_TIMESTAMP_LTZ(ts,3) ," +
            "  WATERMARK FOR et AS et - INTERVAL '0.001' SECOND" +
            ") with (" +
            " 'connector' = 'filesystem', " +
            " 'path' = 'data/ws.json', " +
            " 'format' = 'json'  " +
            ")";

        env.executeSql(createTableSql);
        env.sqlQuery("select * from t1  order by vc,pt  limit 1 ").execute().print();




    }
}

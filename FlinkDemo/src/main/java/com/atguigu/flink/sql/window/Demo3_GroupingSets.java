package com.atguigu.flink.sql.window;

import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.TableEnvironment;

/**
 * Created by Smexy on 2023/7/25
 *
 *   增强聚合。
 */
public class Demo3_GroupingSets
{
    public static void main(String[] args) {

        EnvironmentSettings environmentSettings = EnvironmentSettings.newInstance().inBatchMode().build();
        TableEnvironment env = TableEnvironment.create(environmentSettings);

        String createTableSql = " create table t1 ( id STRING, ts BIGINT, vc INT " +
            "   ) with (" +
            " 'connector' = 'filesystem', " +
            " 'path' = 'data/ws.json', " +
            " 'format' = 'json'  " +
            ")";

        env.executeSql(createTableSql);

        /*
            按照id 求 sum(vc)
            按照id,ts 求sum(vc)
            按照ts 求 sum(vc)

         */

       /* env.sqlQuery("select  id, ts,  sum(vc)" +
               " from t1 " +
               " group by " +
               " grouping sets ( (id), (ts) , (id,ts)  ) "
        ).execute().print();*/

           /*

           rollup(内卷):  rollup( a,b,c )
                        从右至左依次减少维度，进行分组
                    group by a,b,c
                    union all
                    group by a,b
                    union all
                    group by a
                    union all
                    group by

            cube:  多维立方体。 组合方案总数是 2的n次方
                        n是维度。

                    cube(a,b,c)
                        三个维度的组合方案:  group by a,b,c
                        二个维度的组合方案:  group by a,b
                                           group by a,c
                                           group by b,c
                        一个维度的组合方案: group by a
                                        group by b
                                        group by c
                        零个维度的组合方案:  group by
            */

        env.sqlQuery("select ts,id, sum(vc)" +
               " from t1 " +
               " group by " +
              // " rollup( id, ts, vc  ) "
               " cube( id, ts) "
           )
           .execute().print();

    }
}

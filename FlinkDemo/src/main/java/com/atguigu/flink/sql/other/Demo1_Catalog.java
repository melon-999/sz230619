package com.atguigu.flink.sql.other;

import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.TableEnvironment;

/**
        catalog: 目录。管理数据源中的表。

        mysql:   gmall库  表t1
                catalog: mysql
        oracle:  gmall库  表t1
                 catalog: oracle

    --------------
        catalog是在库上添加一层目录，用来声明数据源。

        默认使用GenericInMemoryCatalog管理元数据。一旦程序停止，之前创建的表，这些元数据就丢失了。


 */
public class Demo1_Catalog
{
    public static void main(String[] args) {

        EnvironmentSettings environmentSettings = EnvironmentSettings.newInstance().inStreamingMode().build();
        TableEnvironment tableEnvironment = TableEnvironment.create(environmentSettings);

        System.out.println(tableEnvironment.getCurrentDatabase());  //default_database
        System.out.println(tableEnvironment.getCurrentCatalog());  //default_catalog

        String createTableSql = " create table t1 ( id STRING, ts BIGINT, vc INT  ) with (" +
            " 'connector' = 'filesystem', " +
            " 'path' = 'data/ws.json', " +
            " 'format' = 'json'  " +
            ")";

        //tableEnvironment.executeSql(createTableSql);
        //tableEnvironment.sqlQuery(" select * from t1   ").execute().print();
        tableEnvironment.sqlQuery(" select * from default_catalog.default_database.t1   ").execute().print();

    }
}

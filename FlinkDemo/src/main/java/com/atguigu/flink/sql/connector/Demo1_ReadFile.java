package com.atguigu.flink.sql.connector;

import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.TableEnvironment;

/**
 * Created by Smexy on 2023/11/20
 */
public class Demo1_ReadFile
{
    public static void main(String[] args) {

        //读取外部系统直接获取一个Table，无需流的环境。直接定义表的环境
        EnvironmentSettings settings = EnvironmentSettings.newInstance().inStreamingMode().build();
        TableEnvironment tableEnv = TableEnvironment.create(settings);

        //定义建表语句
        String createTableSql = "CREATE TABLE t1 (" +
            "  id STRING," +
            "  ts BIGINT," +
            "  vc INT " +
            ")  WITH (" +
            "  'connector' = 'filesystem',   " +
            "  'path' = 'data/ws.json', " +
            "  'format' = 'json'             " +
            ")";

        //insert，create语句，需要使用executeSql执行
        tableEnv.executeSql(createTableSql);

        //执行查询
        tableEnv.sqlQuery("select * from t1")
            .execute()
            .print();

        //无需写 tableEnv.execute() 流的环境才需要写;


    }
}

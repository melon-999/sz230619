package com.atguigu.flink.sql.other;

import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.TableEnvironment;

/**
 * Created by Smexy on 2023/7/25
 *
 *  https://nightlies.apache.org/flink/flink-docs-release-1.17/docs/dev/table/sql/queries/set-ops/
 */
public class Demo5_CollectionOperator
{
    public static void main(String[] args) {

        //批处理模式
        EnvironmentSettings environmentSettings = EnvironmentSettings.newInstance().inBatchMode().build();
        TableEnvironment env = TableEnvironment.create(environmentSettings);

        //快速构造一张表(用于测试):  create view 表名(列) as values 一行,一行,一行
        env.executeSql("create view t1(s) as values ('c'), ('a'), ('b'), ('b'), ('c')");
        env.executeSql("create view t2(s) as values ('d'), ('e'), ('a'), ('b'), ('b')");

        //带all，都不去重
        //union： 并集
        //env.sqlQuery(" select * from t1 union select * from t2").execute().print();
        //env.sqlQuery(" select * from t1 union all select * from t2").execute().print();

        //INTERSECT： 交集
        //env.sqlQuery(" select * from t1 INTERSECT select * from t2").execute().print();
        //env.sqlQuery(" select * from t1 INTERSECT all select * from t2").execute().print();

        //差集 看顺序
        env.sqlQuery(" select * from t1 EXCEPT   select * from t2").execute().print();
        env.sqlQuery(" select * from t1 EXCEPT  all select * from t2").execute().print();


    }
}

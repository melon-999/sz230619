package com.atguigu.flink.watermark;

import com.atguigu.flink.function.WaterSensorMapFunction;
import com.atguigu.flink.pojo.WaterSensor;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;

/**
 * Created by Smexy on 2023/11/15
 */
public class Demo6_MultiParilisim
{
    public static void main(String[] args) throws Exception {

        Configuration conf = new Configuration();
        conf.setInteger("rest.port", 3333);
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);

        env.setParallelism(2);

        WatermarkStrategy<WaterSensor> watermarkStrategy = WatermarkStrategy
            .<WaterSensor>forMonotonousTimestamps()
            .withTimestampAssigner( (e, ts) -> e.getTs());

        env
            .socketTextStream("hadoop102", 8888)
            .map(new WaterSensorMapFunction())
            //基于你定义的策略生成水印
            .assignTimestampsAndWatermarks(watermarkStrategy)
            .keyBy(WaterSensor::getId)
            .process(new KeyedProcessFunction<String, WaterSensor, String>()
            {
                @Override
                public void processElement(WaterSensor value, Context ctx, Collector<String> out) throws Exception {
                    //currentWatermark(): 当前数据到达时算子的水印。是更新前的水印。距离最新的水印落后一个身位
                    out.collect(value+"="+ctx.timerService().currentWatermark());
                }
            })
            .print();

        env.execute();
    }
}

package com.atguigu.flink.watermark;

import com.atguigu.flink.function.WaterSensorMapFunction;
import com.atguigu.flink.pojo.WaterSensor;
import com.atguigu.flink.utils.MyUtil;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.time.Duration;

/**
 * Created by Smexy on 2023/11/15
 *

    在一些场景下，由于上游的某些Task的水印不推进，造成下游的算子受连累，无法触发运算。
        此时，可以把长期不推进水印的算子，赋闲回家。
 */
public class Demo7_Idleness
{
    public static void main(String[] args) throws Exception {

        Configuration conf = new Configuration();
        conf.setInteger("rest.port", 3333);
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);

        env.getConfig().setAutoWatermarkInterval(2000);
        env.setParallelism(6);

        WatermarkStrategy<WaterSensor> watermarkStrategy = WatermarkStrategy
            .<WaterSensor>forMonotonousTimestamps()
            .withTimestampAssigner( (e, ts) -> e.getTs())
            //10s不推进水印，就赋闲回家。如果你幡然醒悟，又开始干活(推进水印)，重新启用你
            .withIdleness(Duration.ofSeconds(10))
            ;

        env
            .socketTextStream("hadoop102", 8888)
            .global()
            .map(new WaterSensorMapFunction())
            //基于你定义的策略生成水印
            .assignTimestampsAndWatermarks(watermarkStrategy)
            .keyBy(WaterSensor::getId)
            // 第一个窗口: [0,4999]
            .window(TumblingEventTimeWindows.of(Time.seconds(5)))
            .process(new ProcessWindowFunction<WaterSensor, String, String, TimeWindow>()
            {
                @Override
                public void process(String s, Context context, Iterable<WaterSensor> elements, Collector<String> out) throws Exception {
                    TimeWindow window = context.window();
                    out.collect(window + ":" + MyUtil.parseToList(elements));
                }
            })
            .print();

        env.execute();
    }
}

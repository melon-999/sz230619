package com.atguigu.flink.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Smexy on 2023/11/13
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderEvent
{
    private String oId;
    private String type;
    private String txId;
    private Long ts;
}

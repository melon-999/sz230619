package com.atguigu.flink.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserBehavior
{
    private String userId;
    private String itemId;
    private String categoryId;
    private String behavior;
    private Long ts;
}
package com.atguigu.flink.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Smexy on 2023/11/13
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TxEvent
{
    private String txId;
    private String channel;
    private Long ts;
}

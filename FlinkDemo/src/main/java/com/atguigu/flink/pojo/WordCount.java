package com.atguigu.flink.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Smexy on 2023/11/8
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WordCount
{
    private String word;
    private Integer count;
}

package com.atguigu.jedis;

import redis.clients.jedis.Jedis;

/**
 * Created by Smexy on 2023/11/4

 NoSql，不支持JDBC。
    连接数据库的套路：
        ①创建客户端
        ②使用客户端发送命令
        ③查询操作，接收返回的结果
        ④关闭客户端

 */
public class JedisDemo
{
    public static void main(String[] args) {

        Jedis jedis = new Jedis("hadoop102", 6379);

        //使用
        String msg = jedis.ping();
        System.out.println(msg);

        //String操作
        jedis.set("haha","xixi");
        System.out.println(jedis.get("haha"));
        // String类型的key不存在，返回null，需要对String类型查询的value进行null值判断
        System.out.println("获取不存在的String类型的数据:"+jedis.get("hahaha"));

        //set操作
        jedis.sadd("myset","a","a","b","c");
        System.out.println(jedis.smembers("myset"));
        // Set类型的key不存在，返回[]，无需对返回值进行null值判断
        System.out.println("获取不存在的Set类型的数据:"+jedis.smembers("hahaha"));

        //关闭
        jedis.close();

    }
}

package com.atguigu.jedis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * Created by Smexy on 2023/11/4
 */
public class JedisPoolConfigDemo
{
    public static void main(String[] args) {

        //定制连接池的参数
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        //池子的最大容量
        jedisPoolConfig.setMaxTotal(20);
        //池子中最小存活的连接数
        jedisPoolConfig.setMinIdle(5);
        //池子中最多存活的连接数
        jedisPoolConfig.setMaxIdle(10);
        //阻塞时等待的最大时间，超过这个时间，依旧无法获取连接，就抛异常
        jedisPoolConfig.setMaxWaitMillis(60000);
        //客户端来借连接了，但是连接耗尽了，客户端要不要等一等(阻塞)
        jedisPoolConfig.setBlockWhenExhausted(true);
        //借连接时，先测试下好使，再借
        jedisPoolConfig.setTestOnBorrow(true);
        //还连接时，先测试下好使，再放入池子
        jedisPoolConfig.setTestOnReturn(true);

        //创建一个连接池
        JedisPool jedisPool = new JedisPool(jedisPoolConfig,"hadoop102", 6379);

        //从池中借一个连接
        Jedis jedis = jedisPool.getResource();

        //使用
        System.out.println(jedis.ping());

        //归还连接
        jedis.close();

    }
}

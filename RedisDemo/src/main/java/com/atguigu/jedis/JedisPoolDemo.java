package com.atguigu.jedis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * Created by Smexy on 2023/11/4
 */
public class JedisPoolDemo
{
    public static void main(String[] args) {

        //创建一个连接池
        JedisPool jedisPool = new JedisPool("hadoop102", 6379);

        //从池中借一个连接
        Jedis jedis = jedisPool.getResource();

        //使用
        System.out.println(jedis.ping());

        //归还连接
        jedis.close();

    }
}

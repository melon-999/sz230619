package com.atguigu.dga.util;

import com.atguigu.dga.meta.bean.TableMetaInfo;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Smexy on 2023/10/30
 */
@Component
@Data
public class CacheUtil
{

    /*
        都使用Map作为缓存
            K: 库名.表名
            V: TableMetaInfo
     */
    private Map<String, TableMetaInfo> tableMetaInfoMap = new HashMap<>();

    public  static String getKey(TableMetaInfo metaInfo) {
        return metaInfo.getSchemaName() + "." + metaInfo.getTableName();
    }
}

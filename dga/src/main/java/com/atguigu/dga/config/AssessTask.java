package com.atguigu.dga.config;

import com.atguigu.dga.assess.service.GovernanceAssessDetailService;
import com.atguigu.dga.meta.service.TableMetaInfoExtraService;
import com.atguigu.dga.meta.service.TableMetaInfoService;
import com.atguigu.dga.score.service.CalScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

/**
 * Created by Smexy on 2023/11/3
 */
@Component
public class AssessTask
{
    @Value("${assessDbs}")
    private String dbs;
    //采集元数据
    @Autowired
    private TableMetaInfoService metaInfoService;
    @Autowired
    private TableMetaInfoExtraService extraService;
    //考评
    @Autowired
    private GovernanceAssessDetailService detailService;
    //算分
    @Autowired
    private CalScoreService calScoreService;
    //完成整个评估流程

    public void doAssess(String assessDate) throws Exception {
        //读取配置的要考评的库
        String[] dbs = this.dbs.split(",");
        for (String db : dbs) {
            metaInfoService.initDBMetaInfo(db,assessDate);
            extraService.initMetaInfoExtra(db,assessDate);
            detailService.generateAssessDetail(db,assessDate);
        }
        calScoreService.calScore(assessDate);
    }

    @Scheduled(cron = "0 30 8 * * *")
    public void scheduledAssess() throws Exception {
        String assessDate = LocalDate.now().toString();
        //读取配置的要考评的库
        String[] dbs = this.dbs.split(",");
        for (String db : dbs) {
            metaInfoService.initDBMetaInfo(db,assessDate);
            extraService.initMetaInfoExtra(db,assessDate);
            detailService.generateAssessDetail(db,assessDate);
        }
        calScoreService.calScore(assessDate);
    }
}

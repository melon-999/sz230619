package com.atguigu.dga.assess.service;

import com.atguigu.dga.assess.bean.TDsTaskInstance;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author atguigu
 * @since 2023-10-31
 */
public interface TDsTaskInstanceService extends IService<TDsTaskInstance> {

}

package com.atguigu.dga.assess.bean;

import com.atguigu.dga.meta.bean.TableMetaInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Smexy on 2023/10/28
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AssessParam
{
   private TableMetaInfo metaInfo;
   private GovernanceMetric metric;
   private String assessDate;
}

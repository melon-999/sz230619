package com.atguigu.dga.assess.assessor.storage;

import com.atguigu.dga.assess.assessor.AssessorTemplate;
import com.atguigu.dga.assess.bean.AssessParam;
import com.atguigu.dga.assess.bean.GovernanceAssessDetail;
import com.atguigu.dga.meta.bean.TableMetaInfo;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * Created by Smexy on 2023/10/30
 */
@Component("IS_EMPTY_TABLE")
public class CheckEmptyTable extends AssessorTemplate
{
    @Override
    protected void assess(AssessParam param, GovernanceAssessDetail detail) {
        //获取当前表的总大小
        TableMetaInfo metaInfo = param.getMetaInfo();
        Long tableSize = metaInfo.getTableSize();
        //判断
        if (tableSize == 0){
            assessScore(BigDecimal.ZERO,"当前表是空表!",null,detail,false,null);
        }
    }
}

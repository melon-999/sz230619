package com.atguigu.dga.assess.assessor.spec;

import com.atguigu.dga.assess.assessor.AssessorTemplate;
import com.atguigu.dga.assess.bean.AssessParam;
import com.atguigu.dga.assess.bean.GovernanceAssessDetail;
import com.atguigu.dga.assess.bean.GovernanceMetric;
import com.atguigu.dga.meta.bean.TableMetaInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * Created by Smexy on 2023/10/28
 */
@Component("HAVE_TEC_OWNER")
public class IfHadTecOwnner extends AssessorTemplate
{
    @Override
    protected void assess(AssessParam param, GovernanceAssessDetail detail) {
        TableMetaInfo metaInfo = param.getMetaInfo();
        //isBlank(s): s为null或''或白字符(看不见的空白的字符，空格，回车，tab)，返回true
        if (StringUtils.isBlank(metaInfo.getTableMetaInfoExtra().getTecOwnerUserName())){
            assessScore(BigDecimal.ZERO,"缺少技术负责人!",null,detail,true,metaInfo.getId());
            System.out.println(detail);
        }

    }
}

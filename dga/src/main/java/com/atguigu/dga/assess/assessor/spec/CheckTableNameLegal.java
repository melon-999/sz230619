package com.atguigu.dga.assess.assessor.spec;

import com.atguigu.dga.assess.assessor.AssessorTemplate;
import com.atguigu.dga.assess.bean.AssessParam;
import com.atguigu.dga.assess.bean.GovernanceAssessDetail;
import com.atguigu.dga.config.MetaConstant;
import com.atguigu.dga.meta.bean.TableMetaInfo;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Smexy on 2023/10/28

 参考建数仓表规范
 ODS层 ：开头:ods  结尾 :inc/full
 结构ods_xx_( inc|full)
 DIM层 :  dim开头     full/zip 结尾
 结构: dim_xx_( full/zip)
 DWD层:  dwd 开头  inc/full 结尾
 结构： dwd_xx_xx_(inc|full)
 DWS层： dws开头
 结构dws_xx_xx_xx_ (1d/nd/td)
 ADS层： ads 开头
 结构 ads_xxx
 符合则 10分，否则0分
 OTHER：
 未纳入分层，给5分
 */
@Component("TABLE_NAME_STANDARD")
public class CheckTableNameLegal extends AssessorTemplate
{
    @Override
    protected void assess(AssessParam param, GovernanceAssessDetail detail) {
        TableMetaInfo metaInfo = param.getMetaInfo();
        String schemaName = metaInfo.getSchemaName();
        String tableName = metaInfo.getTableName();
        String dwLevel = metaInfo.getTableMetaInfoExtra().getDwLevel();
        Long id = metaInfo.getId();

        //负责人忘记设置表所属的层级
        if (MetaConstant.DW_LEVEL_UNSET.equals(dwLevel)){
            assessScore(BigDecimal.ZERO,"没有设置表的所属层级",null,detail,true,id);
        }else if (MetaConstant.DW_LEVEL_OTHER.equals(dwLevel) ){
            assessScore(BigDecimal.valueOf(5),"表没有纳入分层",tableName,detail,true,id);
        }else if (MetaConstant.DW_LEVEL_ODS.equals(dwLevel) && MetaConstant.schema_name.equals(schemaName)){
            checkDwLevel(MetaConstant.ODS_REGEX,tableName,detail,id);
        }else if (MetaConstant.DW_LEVEL_DIM.equals(dwLevel) && MetaConstant.schema_name.equals(schemaName)){
            checkDwLevel(MetaConstant.DIM_REGEX,tableName,detail,id);
        }else if (MetaConstant.DW_LEVEL_DWD.equals(dwLevel) && MetaConstant.schema_name.equals(schemaName)){
            checkDwLevel(MetaConstant.DWD_REGEX,tableName,detail,id);
        }else if (MetaConstant.DW_LEVEL_ADS.equals(dwLevel) && MetaConstant.schema_name.equals(schemaName)){
            checkDwLevel(MetaConstant.ADS_REGEX,tableName,detail,id);
        }else if (MetaConstant.DW_LEVEL_DWS.equals(dwLevel) && MetaConstant.schema_name.equals(schemaName)){
            checkDwLevel(MetaConstant.DWS_REGEX,tableName,detail,id);
        }

    }

    private void checkDwLevel(String regex,String tableName,GovernanceAssessDetail detail,Long id){
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(tableName);
       if (!matcher.matches()){
           assessScore(BigDecimal.ZERO,"表名不规范!",tableName,detail,true,id);
       };
    }
}

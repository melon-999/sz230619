package com.atguigu.dga.assess.assessor.storage;

import com.alibaba.fastjson.JSON;
import com.atguigu.dga.assess.assessor.AssessorTemplate;
import com.atguigu.dga.assess.bean.AssessParam;
import com.atguigu.dga.assess.bean.GovernanceAssessDetail;
import com.atguigu.dga.config.MetaConstant;
import com.atguigu.dga.meta.bean.TableMetaInfo;
import com.atguigu.dga.meta.bean.TableMetaInfoExtra;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * Created by Smexy on 2023/10/30

 未设定周期类型的 给 0分
 周期类型为永久、拉链表 则给10分
 周期类型为日分区 :
 无分区信息的给0分
 没设生命周期给0分
 周期长度超过建议周期天数{days}给5分
 */
@Component("LIFECYCLE_ELIGIBLE")
public class CheckLifeCycle extends AssessorTemplate
{
    @Override
    protected void assess(AssessParam param, GovernanceAssessDetail detail) {

        //获取周期类型
        TableMetaInfo metaInfo = param.getMetaInfo();
        TableMetaInfoExtra tableMetaInfoExtra = metaInfo.getTableMetaInfoExtra();
        String lifecycleType = tableMetaInfoExtra.getLifecycleType();
        //获取设置的生命周期天数
        Long lifecycleDays = tableMetaInfoExtra.getLifecycleDays();
        //获取分区字段
        String partitionColNameJson = metaInfo.getPartitionColNameJson();
        //获取日分区表数据保存的建议天数
        Integer recommandDays = getIntegerValueFromConfig(param,"days");

        //开始判断
        if (MetaConstant.LIFECYCLE_TYPE_UNSET.equals(lifecycleType)){
            assessScore(BigDecimal.ZERO,"未设置周期类型",null,detail,true,metaInfo.getId());
        }else if (MetaConstant.LIFECYCLE_TYPE_DAY.equals(lifecycleType)){
            //进行日分区的判断逻辑
            if ("[]".equals(partitionColNameJson)){
                assessScore(BigDecimal.ZERO,"未设置分区字段",null,detail,true,metaInfo.getId());
            }else if (lifecycleDays == -1){
                assessScore(BigDecimal.ZERO,"未设置生命周期保存的天数",null,detail,true,metaInfo.getId());
            }else if (lifecycleDays >  recommandDays){
                assessScore(BigDecimal.valueOf(5),"生命周期超过建议保存的天数："+recommandDays,"当前表的生命周期天数:"+lifecycleDays,
                    detail,true,metaInfo.getId());
            }
        }
    }
}

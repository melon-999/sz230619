package com.atguigu.dga.assess.assessor.spec;

import com.alibaba.fastjson.JSON;
import com.atguigu.dga.assess.assessor.AssessorTemplate;
import com.atguigu.dga.assess.bean.AssessParam;
import com.atguigu.dga.assess.bean.GovernanceAssessDetail;
import com.atguigu.dga.assess.bean.MyFileld;
import com.atguigu.dga.meta.bean.TableMetaInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Smexy on 2023/10/28

   评分标准: 有备注字段/所有字段 *10分

 */
@Component("HAVE_FIELD_COMMENT")
public class CheckFieldMissComment extends AssessorTemplate
{
    @Override
    protected void assess(AssessParam param, GovernanceAssessDetail detail) {
        TableMetaInfo metaInfo = param.getMetaInfo();
        //获取表的字段信息
        String colNameJson = metaInfo.getColNameJson();
        List<MyFileld> myFilelds = JSON.parseArray(colNameJson, MyFileld.class);
        //过滤出缺少comment的字段集合
        List<String> missCommentFieldNames = myFilelds.stream().filter(f -> StringUtils.isBlank(f.getComment())).map(f -> f.getName()).collect(Collectors.toList());

        if (!missCommentFieldNames.isEmpty()){
            BigDecimal score = BigDecimal.valueOf(myFilelds.size() - missCommentFieldNames.size())
                                              .divide(BigDecimal.valueOf(myFilelds.size()), 2, RoundingMode.HALF_UP)
                                              .movePointRight(1);
            assessScore(score,"有字段缺少备注",missCommentFieldNames.toString(),detail,false,null);
        }
    }
}

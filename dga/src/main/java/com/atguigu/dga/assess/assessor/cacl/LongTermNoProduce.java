package com.atguigu.dga.assess.assessor.cacl;

import com.atguigu.dga.assess.assessor.AssessorTemplate;
import com.atguigu.dga.assess.bean.AssessParam;
import com.atguigu.dga.assess.bean.GovernanceAssessDetail;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Smexy on 2023/10/30

 一张表{days}天内没有产出数据  则给0分，其余给10

    经常写java代码，其实有很多非常实用的java工具库:
        apache.commmons.lang3
            字符串工具类: StringUtils
            日期工具类:  DateUtils
        guava:  google java
            集合工具类: Sets
 */
@Component("LONGTERM_NO_PRODUCE")
public class LongTermNoProduce extends AssessorTemplate
{

    @Override
    protected void assess(AssessParam param, GovernanceAssessDetail detail) throws ParseException {
        //获取阈值
        Integer days = getIntegerValueFromConfig(param, "days");
        //获取考评日期
        String assessDate = param.getAssessDate();
        //表的数据是存放在HDFS的表目录中，如果x天产出了数据，此时表目录中文件的lass modifytime是当天
        //获取表的最后一次写入数据的时间
        Timestamp tableLastModifyTime = param.getMetaInfo().getTableLastModifyTime();
        //将考评日期转换为 时间戳  默认 yyyy-MM-dd
        Date date = DateUtils.parseDate(assessDate,"yyyy-MM-dd");
        long assessTime = date.getTime();
        //做差
        long diffMs = Math.abs(assessTime - tableLastModifyTime.getTime());
        //时间单位转换  把相差的毫秒 转换为 天
        long diffDay = TimeUnit.DAYS.convert(diffMs, TimeUnit.MILLISECONDS);
        //判断
        if (diffDay > days){
            assessScore(BigDecimal.ZERO,"长时间未产生数据",
                "最近一次写入的时间:"+tableLastModifyTime+"，已经"+diffDay+"天未产生数据",detail,false,null);
        }

    }
}

package com.atguigu.dga.assess.assessor.cacl;

import com.atguigu.dga.assess.assessor.AssessorTemplate;
import com.atguigu.dga.assess.bean.AssessParam;
import com.atguigu.dga.assess.bean.GovernanceAssessDetail;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * Created by Smexy on 2023/10/30

 一张表{days}天内没有访问 则给0分 ， 其余给10


 LocalDate(日期) 和 LocalDateTime(日期+时间) 都是 java8以后才有的
    使用：
            都是静态方法。
            线程安全。
            可以作为方法的形参，或属性都可以

 Date(日期) : java8之前就有
            都是实例方法。
            线程不安全。
            不能作为属性，不能共享。只能在方法{}中使用！
 */
@Component("LONGTERM_NO_ACCESS")
public class LongTermNoAccess extends AssessorTemplate
{
    @Override
    protected void assess(AssessParam param, GovernanceAssessDetail detail) throws ParseException {
        //获取阈值
        Integer days = getIntegerValueFromConfig(param, "days");
        //获取考评日期
        String assessDate = param.getAssessDate();
        //获取最后一次访问时间
        Timestamp lastAccessTime = param.getMetaInfo().getTableLastAccessTime();
        //时间的转换  日期str ----->LocalDate(日期) ------> LocalDateTime(日期+时间)
        LocalDate localDate = LocalDate.parse(assessDate);
        LocalDateTime assessDateTime = localDate.atStartOfDay();

        //把最后一次访问的时间转换为 LocalDateTime
        LocalDateTime lastAccessDateTime
            = LocalDateTime.ofInstant(Instant.ofEpochMilli(lastAccessTime.getTime()), ZoneId.of("Asia/Shanghai"));

        /*
                |
                |  最后一次访问时间 2023-05-31 01:01:01
                |  当前的考评日期 - days(3) :  2023-10-27 00:00:00
                |
                |  当前的考评日期 2023-10-30 00:00:00
               \/
         */
        if (lastAccessDateTime.isBefore(assessDateTime.minusDays(days))) {
            assessScore(BigDecimal.ZERO,"长时间未访问",
                "最近一次访问的时间:"+lastAccessDateTime+",距离今天的考评日期"+assessDateTime +"，超过了"+days
                ,detail,false,null);

        }
    }
}

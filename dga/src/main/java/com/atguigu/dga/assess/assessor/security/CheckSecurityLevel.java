package com.atguigu.dga.assess.assessor.security;

import com.atguigu.dga.assess.assessor.AssessorTemplate;
import com.atguigu.dga.assess.bean.AssessParam;
import com.atguigu.dga.assess.bean.GovernanceAssessDetail;
import com.atguigu.dga.config.MetaConstant;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * Created by Smexy on 2023/11/2

 未设置 0分  其余10分
 */
@Component("IS_SAFE_LEVEL_SET")
public class CheckSecurityLevel extends AssessorTemplate
{
    @Override
    protected void assess(AssessParam param, GovernanceAssessDetail detail) throws Exception {
        String securityLevel = param.getMetaInfo().getTableMetaInfoExtra().getSecurityLevel();
        Long id = param.getMetaInfo().getId();
        if (MetaConstant.SECURITY_LEVEL_UNSET.equals(securityLevel)){
            assessScore(BigDecimal.ZERO,"未设置安全级别","",detail,true,id);
        }
    }
}

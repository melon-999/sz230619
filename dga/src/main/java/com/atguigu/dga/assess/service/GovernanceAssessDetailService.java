package com.atguigu.dga.assess.service;

import com.atguigu.dga.assess.bean.GovernanceAssessDetail;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.concurrent.ExecutionException;

/**
 * <p>
 * 治理考评结果明细 服务类
 * </p>
 *
 * @author atguigu
 * @since 2023-10-28
 */
public interface GovernanceAssessDetailService extends IService<GovernanceAssessDetail> {


    /**
     *
     * @param db 考评的库，会对库下所有的表进行考评
     * @param assessDate 考评日期。一天一考评
     */
    void generateAssessDetail(String db,String assessDate) throws ExecutionException, InterruptedException;
}

package com.atguigu.dga.meta.service;

import com.atguigu.dga.meta.bean.PageTableMetaInfo;
import com.atguigu.dga.meta.bean.TableMetaInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.hadoop.hive.metastore.api.MetaException;

import java.util.List;

/**
 * <p>
 * 元数据表 服务类
 * </p>
 *
 * @author atguigu
 * @since 2023-10-27
 */
public interface TableMetaInfoService extends IService<TableMetaInfo> {

    //1.根据库名和考评日期抽取Hive库中的元数据，存入到dga.table_meta_info
    void initDBMetaInfo(String db,String assessDate) throws Exception;

    //2.根据前台传入的库名，表名，层级，查询指定条数的 元数据详情
    List<PageTableMetaInfo> queryDataList(int from, Integer pageSize, String tableName, String schemaName, String dwLevel);

    //3.根据前台传入的库名，表名，层级，查询符合条件元数据的总数
    int queryTotal(int from, Integer pageSize, String tableName, String schemaName, String dwLevel);

    //4.在考评之前，查询某个库某一天需要考评的表的元数据信息
    List<TableMetaInfo> getAllTableMetaInfo(String db, String assessDate);
}

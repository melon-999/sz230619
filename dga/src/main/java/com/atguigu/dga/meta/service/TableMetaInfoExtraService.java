package com.atguigu.dga.meta.service;

import com.atguigu.dga.meta.bean.TableMetaInfoExtra;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.hadoop.hive.metastore.api.MetaException;

/**
 * <p>
 * 元数据表附加信息 服务类
 * </p>
 *
 * @author atguigu
 * @since 2023-10-27
 */
public interface TableMetaInfoExtraService extends IService<TableMetaInfoExtra> {

    //为数据库的表的元数据补充(没有，填上)辅助信息
    void initMetaInfoExtra(String db,String assessDate) throws MetaException;
}

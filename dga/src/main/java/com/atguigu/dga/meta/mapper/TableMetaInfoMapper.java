package com.atguigu.dga.meta.mapper;

import com.atguigu.dga.meta.bean.PageTableMetaInfo;
import com.atguigu.dga.meta.bean.TableMetaInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 元数据表 Mapper 接口
 * </p>
 *
 * @author atguigu
 * @since 2023-10-27
 */
@Mapper
public interface TableMetaInfoMapper extends BaseMapper<TableMetaInfo> {

    List<PageTableMetaInfo> queryDataList(@Param("from") int from,
                                          @Param("pageSize") Integer pageSize,
                                          @Param("tableName") String tableName,
                                          @Param("db") String schemaName,
                                          @Param("dwLevel") String dwLevel);

    int queryTotal(@Param("from") int from,
                   @Param("pageSize") Integer pageSize,
                   @Param("tableName") String tableName,
                   @Param("db") String schemaName,
                   @Param("dwLevel") String dwLevel);

    List<TableMetaInfo> getAllTableMetaInfo(@Param("db") String db, @Param("assessDate") String assessDate);
}

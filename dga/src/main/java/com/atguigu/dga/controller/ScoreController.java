package com.atguigu.dga.controller;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.dga.assess.bean.GovernanceAssessDetail;
import com.atguigu.dga.assess.service.GovernanceAssessDetailService;
import com.atguigu.dga.config.AssessTask;
import com.atguigu.dga.score.bean.GovernanceAssessGlobal;
import com.atguigu.dga.score.bean.GovernanceAssessTecOwner;
import com.atguigu.dga.score.service.GovernanceAssessGlobalService;
import com.atguigu.dga.score.service.GovernanceAssessTecOwnerService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by Smexy on 2023/11/3
 */
@RestController
@RequestMapping("/governance")
public class ScoreController
{
    @Autowired
    private GovernanceAssessGlobalService globalService;

    public String getLatestAssessDate(){
        GovernanceAssessGlobal global = globalService.getOne(
            new QueryWrapper<GovernanceAssessGlobal>()
                .select("max(assess_date) assess_date")

        );
        return global.getAssessDate();
    }
    /*
        {  "assessDate":"2023-04-01" ,"sumScore":90, "scoreList":[20,40,34,55,66] }
        //规范，存储，计算，质量，安全
     */
    @GetMapping("/globalScore")
    public Object getGlobalScore(){
        String assessDate = getLatestAssessDate();
        GovernanceAssessGlobal globalScore = globalService.getOne(
            new QueryWrapper<GovernanceAssessGlobal>()
                .eq("assess_date", assessDate)
        );
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("assessDate",assessDate);
        jsonObject.put("sumScore",globalScore.getScore());
        jsonObject.put("scoreList", Arrays.asList(
            globalScore.getScoreSpec(),
            globalScore.getScoreStorage(),
            globalScore.getScoreCalc(),
            globalScore.getScoreQuality(),
            globalScore.getScoreSecurity()
        ));
        return jsonObject;
    }

    @Autowired
    private GovernanceAssessDetailService detailService;
    @GetMapping("/problemList/{governType}/{pageNo}/{pageSize}")
    public Object getAssessDetail(@PathVariable("governType") String type,
                                  @PathVariable("pageNo") int pageNo,
                                  @PathVariable("pageSize") int size){

        String assessDate = getLatestAssessDate();
        //计算返回数据的起始index
        int from = (pageNo - 1) * size;

        List<GovernanceAssessDetail> details = detailService.list(
            new QueryWrapper<GovernanceAssessDetail>()
                .eq("governance_type", type)
                .eq("assess_date", assessDate)
                .ne("assess_score",10)
                .orderByAsc("id")
                .last(" limit " + from + "," + size)
        );

        return details;
    }

    /*
        {"SPEC":1, "STORAGE":4,"CALC":12,"QUALITY":34,"SECURITY":12}
     */
    @GetMapping("/problemNum")
    public Object getProblemNum(){

        String assessDate = getLatestAssessDate();

        List<GovernanceAssessDetail> details = detailService.list(
            new QueryWrapper<GovernanceAssessDetail>()
                .eq("assess_date", assessDate)
                .groupBy("governance_type")
                .select("governance_type", "count(*) id")
                .ne("assess_score",10)
        );

        JSONObject jsonObject = new JSONObject();
        for (GovernanceAssessDetail detail : details) {
            jsonObject.put(detail.getGovernanceType(),detail.getId());
        }
        return jsonObject;
    }

    @Autowired
    private GovernanceAssessTecOwnerService ownerService;
    /*
        [{"tecOwner":"zhang3" ,"score":99},
          {"tecOwner":"li4" ,"score":98},
         {"tecOwner": "wang5","score":97}   ]
     */
    @GetMapping("/rankList")
    public Object getRankList(){

        String assessDate = getLatestAssessDate();
        /*
            把查询的单行，封装为一个Map。把一行的列名作为key，列值作为value。
                { "tec_owner":"王五","score":20 }
            多行，就是把多个Map放入一个List。
         */
        List<Map<String, Object>> maps = ownerService.listMaps(
            new QueryWrapper<GovernanceAssessTecOwner>()
                .eq("assess_date", assessDate)
                .select("tec_owner tecOwner", "score")
        );

        return maps;
    }

    @Autowired
    private AssessTask assessTask;

    @PostMapping("/assess/{date}")
    public Object doAssess(@PathVariable("date") String assessDate) throws Exception {
        assessTask.doAssess(assessDate);
        return "success";
    }
}

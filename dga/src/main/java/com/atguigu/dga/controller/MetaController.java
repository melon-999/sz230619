package com.atguigu.dga.controller;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.dga.meta.bean.PageTableMetaInfo;
import com.atguigu.dga.meta.bean.TableMetaInfo;
import com.atguigu.dga.meta.bean.TableMetaInfoExtra;
import com.atguigu.dga.meta.service.TableMetaInfoExtraService;
import com.atguigu.dga.meta.service.TableMetaInfoService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Smexy on 2023/10/27
 */
@RestController
@RequestMapping("/tableMetaInfo")
public class MetaController
{
    @Autowired
    private TableMetaInfoService metaInfoService;
    @Autowired
    private TableMetaInfoExtraService metaInfoExtraService;

    @PostMapping("/init-tables/{db}/{date}")
    public Object initMetaInfo(@PathVariable("db") String db, @PathVariable("date") String assessDate) throws Exception {
        //同步元数据
        metaInfoService.initDBMetaInfo(db,assessDate);
        //为同步的元数据补充辅助信息
        metaInfoExtraService.initMetaInfoExtra(db,assessDate);
        return "success";
    }

    /**
     *
     * @param pageNo   页码，第几页
     * @param pageSize  一页查看的数据的行数
     * @param tableName  按照表名模糊查询
     * @param schemaName 指定查询的库名
     * @param dwLevel    指定查询的层级
     * @return
     *
     *  pageNo = 1, pageSize = 10
     *      数据的索引从0开始。
     *      要返回第一页的10条数据。 返回索引为0-9的数据。数据的起始索引是0
     *
     *   pageNo = 2, pageSize = 10
     *       要返回第二页的10条数据。 返回索引为10-19的数据。数据的起始索引是10
     *
     *       要返回的数据的起始索引 = (pageNo - 1) * pageSize
     *
     *  -------------------------
     *      前台要的数据格式是{}, 使用Java中的 JSONObject 或 Map封装。
     *      前台要的数据格式是[], 使用Java中的 JSONArray 或 List封装。
     */
    @GetMapping("/table-list")
    public Object getInfoList(Integer pageNo,
                              Integer pageSize,
                              String tableName,
                              String schemaName,
                              String dwLevel){

        //计算要返回数据的起始索引
        int from = (pageNo - 1) * pageSize;

        //查询数据的详情
        List<PageTableMetaInfo> list = metaInfoService.queryDataList(from, pageSize, tableName, schemaName, dwLevel);

        //查询数据的总数
        int total = metaInfoService.queryTotal(from, pageSize, tableName, schemaName, dwLevel);

        //封装结果
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("total",total);
        jsonObject.put("list",list);

        return jsonObject;
    }

    @GetMapping("/table/{id}")
    public Object queryTableMetaInfo(@PathVariable("id") String id){
        //根据id查询TableMetaInfo
        TableMetaInfo metaInfo = metaInfoService.getById(id);
        //根据库名和表名查询额外的辅助信息
        TableMetaInfoExtra metaInfoExtra = metaInfoExtraService.getOne(new
            QueryWrapper<TableMetaInfoExtra>()
            .eq("table_name", metaInfo.getTableName())
            .eq("schema_name", metaInfo.getSchemaName())
        );
        //组装
        metaInfo.setTableMetaInfoExtra(metaInfoExtra);
        //返回
        return metaInfo;
    }

    @PostMapping("/tableExtra")
    public Object updateMetaInfoExtra(@RequestBody TableMetaInfoExtra metaInfoExtra){
            //为辅助信息生成修改时间
        metaInfoExtra.setUpdateTime(new Timestamp(System.currentTimeMillis()));
            //写入数据库。先获取extraMetaInfo的id，根据库名和表名查询extraMetaInfo的id
        Long id = metaInfoExtraService.getOne(
            new
                QueryWrapper<TableMetaInfoExtra>()
                .eq("table_name", metaInfoExtra.getTableName())
                .eq("schema_name", metaInfoExtra.getSchemaName())
        ).getId();
        //设置id
        metaInfoExtra.setId(id);
            //根据id写入
        metaInfoExtraService.updateById(metaInfoExtra);

        return "success";
    }
}

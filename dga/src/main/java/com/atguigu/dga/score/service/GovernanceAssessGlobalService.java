package com.atguigu.dga.score.service;

import com.atguigu.dga.score.bean.GovernanceAssessGlobal;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 治理总考评表 服务类
 * </p>
 *
 * @author atguigu
 * @since 2023-11-02
 */
public interface GovernanceAssessGlobalService extends IService<GovernanceAssessGlobal> {

    //计算全局得分，写入数据库
    void  generateGlobalScore(String assessDate);
}

package com.atguigu.dga.score.service.impl;

import com.atguigu.dga.score.service.CalScoreService;
import com.atguigu.dga.score.service.GovernanceAssessGlobalService;
import com.atguigu.dga.score.service.GovernanceAssessTableService;
import com.atguigu.dga.score.service.GovernanceAssessTecOwnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Smexy on 2023/11/2
 */
@Service
public class CalScoreServiceImpl implements CalScoreService
{
    @Autowired
    private GovernanceAssessTableService tableService;

    @Autowired
    private GovernanceAssessTecOwnerService ownerService;

    @Autowired
    private GovernanceAssessGlobalService globalService;

    @Override
    public void calScore(String assessDate) {
        tableService.generateTableScore(assessDate);
        ownerService.generateScoreByTecOwner(assessDate);
        globalService.generateGlobalScore(assessDate);
    }
}

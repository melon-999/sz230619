package com.atguigu.dga.score.service.impl;

import com.atguigu.dga.score.bean.GovernanceAssessTable;
import com.atguigu.dga.score.bean.GovernanceAssessTecOwner;
import com.atguigu.dga.score.mapper.GovernanceAssessTecOwnerMapper;
import com.atguigu.dga.score.service.GovernanceAssessTecOwnerService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 技术负责人治理考评表 服务实现类
 * </p>
 *
 * @author atguigu
 * @since 2023-11-02
 */
@Service
public class GovernanceAssessTecOwnerServiceImpl extends ServiceImpl<GovernanceAssessTecOwnerMapper, GovernanceAssessTecOwner> implements GovernanceAssessTecOwnerService {

    @Override
    public void generateScoreByTecOwner(String assessDate) {
        //清空今天已经写入的数据
        remove(new QueryWrapper<GovernanceAssessTecOwner>().eq("assess_date",assessDate));
        //计算分数
        List<GovernanceAssessTecOwner> data = baseMapper.calOwnnerScore(assessDate);
        //写入
        saveBatch(data);
    }
}

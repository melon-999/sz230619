package com.atguigu.dga;

import org.apache.hadoop.hive.metastore.HiveMetaStoreClient;
import org.apache.hadoop.hive.metastore.api.MetaException;
import org.apache.hadoop.hive.metastore.api.Table;
import org.apache.thrift.TException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;

import java.util.List;

@SpringBootTest
class DgaApplicationTests
{
    @Autowired
    private HiveMetaStoreClient client;
    @Test
    void testHiveMetaStoreClient() throws Exception {
        List<String> allTables = client.getAllTables("gmall");
        System.out.println(allTables);

        Table table = client.getTable("gmall", "ods_log_inc");
        System.out.println(table);

    }

}

package com.atguigu.dga;

import com.atguigu.dga.assess.service.GovernanceAssessDetailService;
import com.atguigu.dga.meta.service.TableMetaInfoService;
import com.google.common.collect.Sets;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.permission.FsPermission;
import org.apache.hadoop.hive.metastore.HiveMetaStoreClient;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashSet;
import java.util.OptionalDouble;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by Smexy on 2023/10/28
 */
@SpringBootTest
public class AssessTest
{
    @Autowired
    private TableMetaInfoService metaInfoService;
    @Test
    public void testQueryMetaInfo() throws Exception{
        System.out.println(metaInfoService.getAllTableMetaInfo("gmall", "2023-05-26"));
    }


    @Test
    public void testPermission() throws Exception{
        FileSystem hdfs = context.getBean(FileSystem.class);

        String path = "/warehouse/gmall/dim/dim_user_zip";
        FileStatus fileStatus = hdfs.getFileStatus(new Path(path));

        FsPermission permission = fileStatus.getPermission();
        // d
        System.out.println(permission);
        //把字符串的权限转换为 数值
        /*  user group  other
            rwx  r-x     r-x
            111  101     101
            7    5       5
            有权限，记为1，没有(-)，记为0
         */
        System.out.println(permission.getUserAction().ordinal()*100 + permission.getGroupAction().ordinal()*10 + permission.getOtherAction().ordinal() );

    }

    @Autowired
    private ApplicationContext context;
    @Test
    public void testClient() throws Exception{
        //HiveMetaStoreClient client1 = context.getBean(HiveMetaStoreClient.class);
        //client1.close();
        System.out.println("haha");
        /*HiveMetaStoreClient client2 = context.getBean(HiveMetaStoreClient.class);
        client2.getAllTables("gmall");
        HiveMetaStoreClient client3 = context.getBean(HiveMetaStoreClient.class);
        client3.getAllTables("gmall");*/

    }
    @Autowired
    private GovernanceAssessDetailService detailService;

    @Test
    public void testAssess() throws Exception{
        detailService.generateAssessDetail("gmall","2023-05-26");
    }

    /*
        在很多语言中，都支持以下这些字符串占位符
            %s:  字符串类型的占位符
            %d:  数值类型的占位符

            %: 在当前的环境下，%是有特殊含义的，如果需要编写一个没有特殊含义的%，意味需要对%进行转义。
                    %%: 代表没有特殊含义的%
     */
    @Test
    public void testStringConcat() throws Exception{
            String template = "当前任务运行时效低,%s的运行时间是:%d,超过了过去%d的平均时间%d 的 %d%%";
            //填充占位符
        String result = String.format(template, "2023-05-26", 30, 3, 20, 10);
        System.out.println(result);
    }

    /*
         正则表达式: https://www.runoob.com/regexp/regexp-tutorial.html
            ^： 开头
            $:  结尾
            |:  或
            \w:  匹配任意一个 a-z,A-z,_ 字符
            \d:  匹配任意一个数字0-9
            +: 代表匹配多次

            \: 在java中是特殊字符，如果希望编写\，需要转义。 \\ 才是 \
     */
    @Test
    public void testRegex() throws Exception{

        /*
            定义ods层的正则表达式
                ods_order_inc|full
         */
        //定义的正则表达式字符串
        String ods_pattern = "^ods_\\w+_(inc|full)$";
        //正则表达式对象
        Pattern pattern = Pattern.compile(ods_pattern);
        //定义检测的目标
        //String str = "ods_log_inc";
        String str = "ods_log_ful";

        //使用正则表达式检测字符串是否符合规则 Matcher代表检查结果
        Matcher matcher = pattern.matcher(str);

        if (matcher.matches()){
            System.out.println(str+" 符合ods层的建表命名规则... ");
        }else {
            System.out.println(str+" 不符合ods层的建表命名规则... ");

        }

    }

    @Test
    public void testBigDecimalCal() throws Exception{
         //+
        BigDecimal result = BigDecimal.ZERO.add(BigDecimal.TEN);
        System.out.println(result);
        //-
        BigDecimal result1 = BigDecimal.ZERO.subtract(BigDecimal.TEN);
        System.out.println(result1);
        //*
        BigDecimal result2 = BigDecimal.ZERO.multiply(BigDecimal.TEN);
        System.out.println(result2);
        // * 10的倍数，可以移动小数点，小数点右移，举例 * 100
        System.out.println(BigDecimal.valueOf(1).movePointRight(2));
        // /
        BigDecimal result3 = BigDecimal.ZERO.divide(BigDecimal.TEN);
        System.out.println(result3);
        //  / 10的倍数，可以移动小数点，小数点左侧移，举例 / 100
        System.out.println(BigDecimal.valueOf(100).movePointLeft(2));
        // 除不尽，可以取整
        System.out.println(BigDecimal.valueOf(10).divide(BigDecimal.valueOf(3),2, RoundingMode.HALF_UP));

    }

    /*
        本质上就是set集合取交集
            举例:
                Set1 :  a,b,c
                Set2 :  A,B,c
            看set1和set2重复的部分，就是进行交集运算。


     */
    @Test
    public void testSetInter() throws Exception{
        HashSet<String> set1 = Sets.newHashSet("a", "b", "c");
        HashSet<String> set2 = Sets.newHashSet("d", "b", "c");
        System.out.println("交集运算之前:"+set1);
        //交集
        boolean isInter = set1.retainAll(set2);
        //如果有交集，交集的结果是在set1中存储
        if (isInter){
            System.out.println("交集的结果:"+set1);
        }
    }

    /*
        差集运算:
                Set1 :  a,b,c
                Set2 :  A,B,c
                set1差集运算set2，取set1独有的，set2没有的部分。
     */
    @Test
    public void testSetDiff() throws Exception{
        HashSet<String> set1 = Sets.newHashSet("a", "b", "c");
        HashSet<String> set2 = Sets.newHashSet("d", "b", "c");
        System.out.println("差集运算之前:"+set1);
        //差集
        boolean isDiff = set1.removeAll(set2);
        //如果有交集，交集的结果是在set1中存储
        if (isDiff){
            System.out.println("差集的结果:"+set1);
        }
    }

    @Test
    public void testSetUnion() throws Exception{
        HashSet<String> set1 = Sets.newHashSet("a", "b", "c");
        HashSet<String> set2 = Sets.newHashSet("d", "b", "c");
        System.out.println("并集运算之前:"+set1);
        //并集
        set1.addAll(set2);
        System.out.println("并集的结果:"+set1);

    }

}

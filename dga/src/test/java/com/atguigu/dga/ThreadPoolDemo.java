package com.atguigu.dga;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Smexy on 2023/11/3
 *

 线程创建完之后，运行相关的任务，运行结束要销毁线程。
    频繁地创建和销毁线程，会浪费时间和资源。
    线程池的意义就是在池中提前创建好线程，节省线程的创建时间。
    当线程池中的线程运行完Task后，放回池中，复用。节省了线程的销毁时间。



    springboot容器内置了线程池。无需创建，只需要直接声明，注入即可。

 */
public class ThreadPoolDemo
{
    public static void main(String[] args) {


        //创建一个内置10个线程的池子
        ExecutorService pool = Executors.newFixedThreadPool(10);

        MyThreadTask3 task3 = new MyThreadTask3();
        //使用池子，提交线程
        pool.submit(task3);
        pool.submit(task3);
        pool.submit(task3);
        //最后，关闭线程池
        pool.shutdown();
    }
}

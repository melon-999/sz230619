package com.atguigu.dga;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.StopWatch;

import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Smexy on 2023/11/3

    Thread ---->Runable---->Callable---->CompletableFuture
 */
@SpringBootTest
public class ThreadTest
{
    @Autowired
    private ThreadPoolTaskExecutor pool;
    @Test
    public void testPool() throws Exception{

        StopWatch stopWatch = new StopWatch();
        stopWatch.start(); //开始计时
        //专门用于收集线程任务执行完返回的结果
        CompletableFuture<String> [] tasks = new CompletableFuture [10];

        //一次性提交10个任务。10个任务使用线程池提供的10个线程去跑
        for (int i = 0; i < 10; i++) {
            //提交任务时，是不阻塞的，把任务提交给线程池，就跑。不管后果
            int finalI = i;
            /*
                CompletableFuture<String> result 是任务执行的结果。
                    调用这个对象，获取任务的返回值！
             */
            tasks[i] = CompletableFuture.supplyAsync(new Supplier<String>()
            {
                //和Callable的get是一样的，定义任务逻辑，返回数据
                @Override
                public String get() {
                    System.out.println(Thread.currentThread().getName() +"  dddd");
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    return finalI + " 号任务已经完成!";
                }
            }, pool);
        }

        /*
            allOf(tasks).xxx:  让tasks中的每一个结果对象都调用xxx()
                get(): 等待任务执行完成，返回结果。结果还是存储在tasks数组中。
                        阻塞当前线程
         */
        CompletableFuture.allOf(tasks).get();

        System.out.println("heiheihei");
        //任务跑完，获取结果
        String result = Arrays.stream(tasks)
                               .map(f -> {
                                   try {
                                       return f.get();
                                   } catch (Exception e) {
                                       throw new RuntimeException(e);
                                   }
                               })
                               .collect(Collectors.joining(","));

        System.out.println(result);

        stopWatch.stop(); //停止计时
        System.out.println("总耗时:"+stopWatch.getTotalTimeSeconds());


    }

    @Test
    public void testNoThread() throws Exception{

        StopWatch stopWatch = new StopWatch();
        stopWatch.start(); //开始计时
        for (int i = 0; i < 10; i++) {
            System.out.println(new MyThreadTask3().call());
        }
        stopWatch.stop(); //停止计时
        System.out.println("总耗时:"+stopWatch.getTotalTimeSeconds());
    }

    /*
            线程和并行流的区别
                线程： 数量，是由用户决定。
                            逻辑是自由。

                并行流： 空闲的cpu核心数
                            只能处理Stream(集合)
     */
    @Test
    public void testStream() throws Exception{

        Stream.iterate(1,x -> x+1)
            .parallel()  //把集合切片，并行处理。参考cpu核心数切片
            .limit(100)
            .forEach(x -> {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println(Thread.currentThread().getName() + " xx "+x);
            });

    }
}

package com.atguigu.dga;

import com.atguigu.dga.meta.service.TableMetaInfoExtraService;
import com.atguigu.dga.meta.service.TableMetaInfoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Created by Smexy on 2023/10/27
 */
@SpringBootTest
public class MetaTest
{
    @Autowired
    private TableMetaInfoService metaInfoService;
    @Test
    public void testExtractHiveMeta() throws Exception{
        metaInfoService.initDBMetaInfo("gmall","2023-05-26");
    }

    @Autowired
    private TableMetaInfoExtraService extraService;

    @Test
    public void testInitExtraInfo() throws Exception{
     extraService.initMetaInfoExtra("gmall","2023-05-26");
    }

    @Test
    public void testQueryMetaInfoList() throws Exception{
        System.out.println(metaInfoService.queryDataList(0, 5, "inc", null, "ODS"));
    }

    @Test
    public void testQueryTotal() throws Exception{
        System.out.println(metaInfoService.queryTotal(0, 5, "inc", null, "ODS"));
    }
}

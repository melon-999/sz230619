package com.atguigu.dga;

import com.atguigu.dga.score.service.CalScoreService;
import com.atguigu.dga.score.service.GovernanceAssessGlobalService;
import com.atguigu.dga.score.service.GovernanceAssessTableService;
import com.atguigu.dga.score.service.GovernanceAssessTecOwnerService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Created by Smexy on 2023/11/2
 */
@SpringBootTest
public class ScoreTest
{
    @Autowired
    private GovernanceAssessTableService tableService;
    @Test
    public void testTable() throws Exception{

     tableService.generateTableScore("2023-05-26");

    }

    @Autowired
    private GovernanceAssessTecOwnerService ownerService;
    @Test
    public void testOwnner() throws Exception{

        ownerService.generateScoreByTecOwner("2023-05-26");

    }

    @Autowired
    private GovernanceAssessGlobalService globalService;
    @Test
    public void testGlobal() throws Exception{

        globalService.generateGlobalScore("2023-05-26");

    }

    @Autowired
    private CalScoreService calScoreService;
    @Test
    public void test() throws Exception{
            calScoreService.calScore("2023-05-26");
    }
}

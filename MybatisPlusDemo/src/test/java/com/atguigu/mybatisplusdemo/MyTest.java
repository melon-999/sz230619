package com.atguigu.mybatisplusdemo;

import com.atguigu.mybatisplusdemo.bean.Employee;
import com.atguigu.mybatisplusdemo.mapper.EmployeeMapper;
import com.atguigu.mybatisplusdemo.service.EmployeeService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * Created by Smexy on 2023/10/25
 */
@SpringBootTest
public class MyTest
{
    @Autowired
    private EmployeeService employeeService;
    /*
        查询emp表中id > 2的，gender=male的员工的name,gender 将员工按照id降序排序，之后只取前两名
            list(Wrapper x)
                Wrapper的常见实现:
                    QueryWrapper: 用于查询和删除
                    UpdateWrapper： 用于更新
     */
    @Test
    public void testQuery() throws Exception{

        //查询条件
        QueryWrapper<Employee> queryWrapper = new QueryWrapper<Employee>()
            .eq("gender", "male")
            .gt("id", 2)
            .select("last_name", "gender")
            .orderByDesc("id")
            .last(" limit 2 ");

        List<Employee> result = employeeService
            .list(queryWrapper);

        System.out.println(result);

    }

    @Test
    public void testUpdate() throws Exception{

        //把id > 5的gender 改为 female
        //更新条件
        UpdateWrapper<Employee> updateWrapper = new UpdateWrapper<Employee>()
            .gt("id", 5)
            .set("gender", "female");

        employeeService.update(updateWrapper);


    }

    @Test
    public void testDelete() throws Exception{

        //把id > 5的删除
        //查询条件
        QueryWrapper<Employee> queryWrapper = new QueryWrapper<Employee>()
            .gt("id", 5);

        employeeService.remove(queryWrapper);


    }

    @Autowired
    private EmployeeMapper mapper;
    @Test
    public void testJoin() throws Exception{

        System.out.println(mapper.queryJoinEmps());


    }

}

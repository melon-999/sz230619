package com.atguigu.mybatisplusdemo.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@NoArgsConstructor //为类提供空参构造器
@AllArgsConstructor //为类提供全参构造器
@Data  //为为私有属性提供公共的getter,setter，重写hashcode，toString()
//默认情况，使用Bean的类名作为要操作的表名
@TableName("emp")
public class Employee
{
    //告诉MybatisPlus，Employee所生成的insert sql语句中，无需写入id列，因为id是自增的。
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String lastName;
    private String gender;
    private String email;

    //告诉MybatisPlus这个字段不和表映射，生成sql时，不要携带
    @TableField(exist = false)
    private String haha;

    public static void main(String[] args) {
        Employee employee = new Employee();
        //System.out.println(new Employee(1, "tom", "a", "b"));

    }

}

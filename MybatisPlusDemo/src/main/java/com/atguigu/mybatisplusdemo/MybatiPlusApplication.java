package com.atguigu.mybatisplusdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatiPlusApplication
{

    public static void main(String[] args) {
        SpringApplication.run(MybatiPlusApplication.class, args);
    }

}

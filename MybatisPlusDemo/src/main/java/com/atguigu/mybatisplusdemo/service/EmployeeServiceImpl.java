package com.atguigu.mybatisplusdemo.service;

import com.atguigu.mybatisplusdemo.bean.Employee;
import com.atguigu.mybatisplusdemo.mapper.EmployeeMapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Map;
import java.util.function.Function;

/**
 * Created by Smexy on 2023/10/25
 */
@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper,Employee> implements EmployeeService
{
}

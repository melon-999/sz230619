package com.atguigu.mybatisplusdemo.service;

import com.atguigu.mybatisplusdemo.bean.ActivitySku;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 活动商品关联表 服务类
 * </p>
 *
 * @author tom
 * @since 2023-10-25
 */
public interface ActivitySkuService extends IService<ActivitySku> {

}

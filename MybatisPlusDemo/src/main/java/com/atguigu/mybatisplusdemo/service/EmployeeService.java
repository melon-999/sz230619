package com.atguigu.mybatisplusdemo.service;

import com.atguigu.mybatisplusdemo.bean.Employee;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * Created by Smexy on 2023/10/25
 */
public interface EmployeeService extends IService<Employee>
{

}

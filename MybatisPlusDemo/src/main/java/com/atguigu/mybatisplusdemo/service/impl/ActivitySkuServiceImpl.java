package com.atguigu.mybatisplusdemo.service.impl;

import com.atguigu.mybatisplusdemo.bean.ActivitySku;
import com.atguigu.mybatisplusdemo.mapper.ActivitySkuMapper;
import com.atguigu.mybatisplusdemo.service.ActivitySkuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 活动商品关联表 服务实现类
 * </p>
 *
 * @author tom
 * @since 2023-10-25
 */
@Service
public class ActivitySkuServiceImpl extends ServiceImpl<ActivitySkuMapper, ActivitySku> implements ActivitySkuService {

}

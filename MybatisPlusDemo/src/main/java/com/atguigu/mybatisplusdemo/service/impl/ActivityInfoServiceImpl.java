package com.atguigu.mybatisplusdemo.service.impl;

import com.atguigu.mybatisplusdemo.bean.ActivityInfo;
import com.atguigu.mybatisplusdemo.mapper.ActivityInfoMapper;
import com.atguigu.mybatisplusdemo.service.ActivityInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 活动表 服务实现类
 * </p>
 *
 * @author tom
 * @since 2023-10-25
 */
@Service
public class ActivityInfoServiceImpl extends ServiceImpl<ActivityInfoMapper, ActivityInfo> implements ActivityInfoService {

}

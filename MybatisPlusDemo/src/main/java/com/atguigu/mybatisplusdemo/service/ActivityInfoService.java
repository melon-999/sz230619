package com.atguigu.mybatisplusdemo.service;

import com.atguigu.mybatisplusdemo.bean.ActivityInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 活动表 服务类
 * </p>
 *
 * @author tom
 * @since 2023-10-25
 */
public interface ActivityInfoService extends IService<ActivityInfo> {

}

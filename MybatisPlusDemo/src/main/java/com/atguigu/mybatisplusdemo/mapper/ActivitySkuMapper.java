package com.atguigu.mybatisplusdemo.mapper;

import com.atguigu.mybatisplusdemo.bean.ActivitySku;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 活动商品关联表 Mapper 接口
 * </p>
 *
 * @author tom
 * @since 2023-10-25
 */
@Mapper
public interface ActivitySkuMapper extends BaseMapper<ActivitySku> {

}

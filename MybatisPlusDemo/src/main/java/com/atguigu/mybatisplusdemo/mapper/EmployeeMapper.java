package com.atguigu.mybatisplusdemo.mapper;

import com.atguigu.mybatisplusdemo.bean.Employee;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by Smexy on 2023/10/25
 */
@Mapper
//@DS("mybatis")  不加有默认
public interface EmployeeMapper extends BaseMapper<Employee>
{
    @Select("SELECT" +
        "   t2.*" +
        " FROM" +
        " (SELECT id,last_name FROM emp ) t1" +
        " JOIN" +
        " (SELECT id,last_name FROM emp WHERE id < 3) t2" +
        " ON t1.id = t2.id")
    @DS("mybatis")
    List<Employee> queryJoinEmps();
}

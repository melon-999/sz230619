package com.atguigu.mybatisplusdemo.controller;

import com.atguigu.mybatisplusdemo.bean.Employee;
import com.atguigu.mybatisplusdemo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Smexy on 2023/10/24

 页面发送两种请求:
    请求一:  /emp?op=delete&id=&lastname=&gender=&email=
                根据op的不同，对员工进行CRUD。
            op: select | insert | update |delete

    请求二:  /getAllEmp
                返回所有员工的信息

 */
@RestController
public class EmployeeController
{
    /*
        所有的属性的声明都用接口类型，而不用具体的实现类

         1.现在的接口开发，很多情况下，是微服务的分布式开发。
                我们可能只有约定好的接口，而没有具体的实现

         2.面向接口开发，就强制必须使用接口类型。
                好处： 方便维护！
                多态
                    变量使用父类的引用，但是确实子类的实现。
                    调用父类的功能时，实际是由子类来完成。


     */
    @Autowired
    private EmployeeService employeeService;
    //private EmployeeServiceImpl2 employeeService ;

    @RequestMapping("/emp")
    public Object handle1(String op,Integer id,String lastname,String gender,String email ){

        //接收参数，把参数封装为数据模型Bean
        Employee param = new Employee(id, lastname, gender, email,null);
        //调用Service来处理
        switch (op){
            case "select": if (id == null){
                return "id非法!";
            }else {
                Employee e = employeeService.getById(id);
                return e == null ? "此人不存在" : e;
            }
            case "delete": if (id == null){
                return "id非法!";
            }else {
                employeeService.removeById(id);
                return "删除成功!";
            }

            case "update":  if (id == null){
                return "id非法!";
            }else {
                employeeService.updateById(param);
                return "更新成功!";
            }

            case "insert":  {
                employeeService.save(param);
                return "写入成功!";
            }

            default: return "ok";
        }

    }

    @RequestMapping("/getAllEmp")
    public Object handle2(){

        //调用Service来处理
        return employeeService.list();
    }


}

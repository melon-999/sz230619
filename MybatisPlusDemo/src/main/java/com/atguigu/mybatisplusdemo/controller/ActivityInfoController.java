package com.atguigu.mybatisplusdemo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 活动表 前端控制器
 * </p>
 *
 * @author tom
 * @since 2023-10-25
 */
@RestController
@RequestMapping("/activityInfo")
public class ActivityInfoController {

}

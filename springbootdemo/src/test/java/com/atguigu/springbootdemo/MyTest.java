package com.atguigu.springbootdemo;

import com.atguigu.springbootdemo.bean.Dog;
import com.atguigu.springbootdemo.bean.TianDog;
import com.atguigu.springbootdemo.bean.TuDog;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

/**
 * Created by Smexy on 2023/10/24
 */
@SpringBootTest
public class MyTest
{
    @Value("${name}")
    private String myName;

    @Test
    public void testGetProperty(){
        System.out.println(myName);
    }

    /*
        如何创建对象:
            只需要在类上标注注解，就可以让容器帮我们自动创建对象。
               1. 标注@Controller，@RestController，容器看到后，就会自动在容器中为这个类创建一个单例对象。
               2. 如果类是一个普通类，标注@Component,容器看到后，就会自动在容器中为这个类创建一个单例对象。

     */

    /*
        从容器中获取对象:
            在赋值的变量上标注 @Autowired。

            @Autowired可以根据标注的属性的类型自动从容器中获取对应的类型赋值。
                    类似之前的 =

           --------------
           不使用Spring的玩法:  TuDog tuDog = new TuDog();

     */
    @Autowired
    private TuDog tuDog;

    @Test
    public void testGetBean(){
        System.out.println(tuDog);
    }

    //@Autowired
    private Dog dog;

    /*
        expected single matching bean but found 2: tianDog,tuDog
            如果容器中有多个同一类型的对象，此时 @Autowired会产生歧义。
     */
    @Autowired
    private ApplicationContext context;  //容器
    @Test
    public void testGetBean2(){
        //从容器中取出指定的对象
        dog = context.getBean(TianDog.class);
        System.out.println(dog);
    }
}

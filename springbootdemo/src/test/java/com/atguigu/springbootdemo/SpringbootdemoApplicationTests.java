package com.atguigu.springbootdemo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/*
    使用的是springboot，测试类一定要标注@SpringBootTest。
    标注了@SpringBootTest注解，测试类启动时，才能访问容器！
    springboot的所有功能都离不开容器！
 */
@SpringBootTest
class SpringbootdemoApplicationTests
{

    @Test
    void contextLoads() {
    }

}

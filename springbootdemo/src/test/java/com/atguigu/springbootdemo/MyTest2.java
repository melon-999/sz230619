package com.atguigu.springbootdemo;

import com.atguigu.springbootdemo.bean.Dog;
import com.atguigu.springbootdemo.bean.TianDog;
import com.atguigu.springbootdemo.bean.TuDog;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

import java.time.LocalDate;

/**
 * Created by Smexy on 2023/10/24
 */
@SpringBootTest
public class MyTest2
{

    @Autowired
    @Qualifier("tian")  //自动从容器中找Dog类型，且id(名字)为tu的对象赋值给变量
    private Dog dog;

    private Dog dog2;

    @Autowired
    private ApplicationContext context;  //容器

    @Test
    public void testGetBean2(){
        dog2 = context.getBean("tu", Dog.class);
        System.out.println(dog2);
    }

    /*
        加注解创建对象的方式，只适用于你有源代码的情况。
        没有源代码，可以通过其他的方式！
     */
    @Autowired
    private LocalDate dt;

    @Test
    public void testGetBean3(){
       //创建昨天的日期对象。
        System.out.println(dt);
    }
}

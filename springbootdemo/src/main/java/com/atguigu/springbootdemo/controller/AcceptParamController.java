package com.atguigu.springbootdemo.controller;

import com.atguigu.springbootdemo.bean.Employee;
import com.atguigu.springbootdemo.bean.MyParam;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by Smexy on 2023/10/24

 如何接收请求方传递的参数。
     分类：
            1.普通参数
                    方式一:只需要在处理请求的方法的形参列表中声明和请求方提供的参数名一致的形参，就能接收。
                          类型只要兼容即可。
                           在参数列表中，把多个参数铺开。
                    方式二:如果传递的参数非常多，处理请求的方法的参数列表铺开写不方便。
                            可以使用Bean ： 属性名必须和请求方发送的参数名一致，才能收到
                                或
                            Map接收。 在参数前添加注解@RequestParam

            2.json格式的参数
                    只能使用 apipost工具发送。
                    接收:
                        只能使用Bean(Bean的属性名必须和Json中的属性名一致，才能收到)
                            或Map来接收。
 */
@RestController
public class AcceptParamController
{
    /*
        请求:  http://localhost:8080/sendParam1?name=jack&age=20
     */
    @RequestMapping(value = "/sendParam1")
    public Object handle1(String name,Integer age){
        System.out.println(name + " === " + age);
        return "ok";
    }

    /*
      请求:  http://localhost:8080/sendParam2?name=jack&age=20
   */
    @RequestMapping(value = "/sendParam2")
    public Object handle2(MyParam myParam){
        System.out.println(myParam);
        return "ok";
    }

    /*
     请求:  http://localhost:8080/sendParam3?name=jack&age=20
  */
    @RequestMapping(value = "/sendParam3")
    public Object handle3(@RequestParam Map<String,Object> map){
        System.out.println(map);
        return "ok";
    }

    /*
   请求:  http://localhost:8080/sendJson
    */
    @RequestMapping(value = "/sendJson")
    public Object handle4(@RequestBody Employee e){
        System.out.println(e);
        return "ok";
    }

    /*
  请求:  http://localhost:8080/sendJson2
   */
    @RequestMapping(value = "/sendJson2")
    public Object handle5(@RequestBody Map<String,Object> e){
        System.out.println(e);
        return "ok";
    }

    //在所有的控制器中，都只能有一个方法处理指定的url。否则会产生歧义，app无法启动！
    //错误的师范
    /*@RequestMapping(value = "/sendJson2")
    public Object handle6(@RequestBody Map<String,Object> e){
        System.out.println(e);
        return "ok";
    }*/

    /*
请求:  http://localhost:8080/a/b/c/d
    希望获取c，
            c并不是请求参数，而是路径的一部分。
 */
    @RequestMapping(value = "/a/b/{haha}/d")
    public Object handle7(@PathVariable("haha") String a){
        System.out.println(a);
        return "ok";
    }
}

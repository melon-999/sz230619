package com.atguigu.springbootdemo.controller;

import com.atguigu.springbootdemo.bean.Employee;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Smexy on 2023/10/24
 *

 现在的开发时前后端分离的开发。前端负责页面和数据渲染，后端只负责接收请求，返回数据。

 后端负责接收请求，返回数据，这种操作也称为写(数据)接口！
    所有写接口的方法，都需要标注 @ResponseBody注解，此时会把返回值作为数据进行返回！

 */
//@Controller
@RestController //作用 = @Controller + 为标注类的所有方法自动添加 @ResponseBody
public class DataController
{
    /*
        返回的是一个字面量。直接把字面量响应给请求方。

        要求发送 http://localhost:8080/getData1
        希望返回:  success.html
     */
    //@ResponseBody
    @RequestMapping(value = "/getData1")
    public String handle(){
        System.out.println("xixixi");
        return "success.html";
    }

    /*
       返回的是一个非字面量。
            把非字面量，使用内置JSON解析框架 jackson,转换为json字符串，再响应给请求方。

       要求发送 http://localhost:8080/getData2
       希望返回:  Employee对象
    */
   // @ResponseBody
    @RequestMapping(value = "/getData2")
    public Object handle2(){

        return  new Employee(1, "tom", "a", "b");
    }
}

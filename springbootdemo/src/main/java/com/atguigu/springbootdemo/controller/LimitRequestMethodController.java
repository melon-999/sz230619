package com.atguigu.springbootdemo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Smexy on 2023/10/24
 *
    当限定了请求方式后，只要请求方式不对，就报错405，Method Not Allowed
 */
@RestController
@RequestMapping(value = "/haha") //作用是为这个类所有标注了@RequestMapping的方法的url路径上，添加一层父路径
public class LimitRequestMethodController
{
    //只限定url路径，不限定请求方式。get，post都能处理
    @RequestMapping(value = "/heiheihei")
    public Object handle1(){
            return "heihei";
    }

    //只能发get请求
    @GetMapping(value = "/heiheihei2")
    public Object handle2(){
        return "heihei";
    }

    //只能发post请求
    @PostMapping(value = "/heiheihei3")
    public Object handle3(){
        int i = 1/0;
        return "heihei";
    }
}

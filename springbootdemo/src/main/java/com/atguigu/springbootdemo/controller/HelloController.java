package com.atguigu.springbootdemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Smexy on 2023/10/24
 演示页面请求。

    发送请求: http://localhost:8080/hahaha
    希望程序处理请求，打印xixixi，把success.html返回给请求方！

    能够处理web请求的类，称为控制器。会标注@Controller
 */
@Controller
public class HelloController
{
    /*
        声明这个方法能够处理哪些请求.
            在java代码中编写url，无需携带http://localhost:8080
     */
    @RequestMapping(value = "/hahaha")
    public String handle(){
        System.out.println("xixixi");
        //把resources/static 目录下的指定页面返回给请求方
        return "success.html";
    }
}

package com.atguigu.springbootdemo.service;

import com.atguigu.springbootdemo.bean.Employee;
import com.atguigu.springbootdemo.bean.Region;

import java.util.List;

/**
 * Created by Smexy on 2023/10/24
 */
public interface EmployeeService
{
    //为客户在页面的五种操作提供对应的业务方法
    Employee getEmployeeById(Integer id);

    List<Employee> getAll();

    //增删改可以不要返回值
    void insertEmployee(Employee e);

    void updateEmployee(Employee e);

    void deleteEmployeeById(Integer id);

    void login();

    List<Region> getAllRegion();
}

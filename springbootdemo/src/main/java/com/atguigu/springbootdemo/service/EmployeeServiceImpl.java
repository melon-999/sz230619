package com.atguigu.springbootdemo.service;

import com.atguigu.springbootdemo.bean.Employee;
import com.atguigu.springbootdemo.bean.Region;
import com.atguigu.springbootdemo.mapper.EmployeeMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Smexy on 2023/10/24
 *

 @Service的作用：
    1.给程序员看，说明这个类是一个业务模型
    2.给容器看，容器会为这个注解标注的类，自动在容器中创建一个单例对象
 */
//@Service
public class EmployeeServiceImpl implements EmployeeService
{
    //准备一个Dao
    @Autowired
    private EmployeeMapper mapper;
    @Override
    public Employee getEmployeeById(Integer id) {
        System.out.println("操作之前要做.....");
        Employee e = mapper.getEmployeeById(id);
        System.out.println("操作完成后要做.....");
        return e;
    }

    @Override
    public List<Employee> getAll() {
        System.out.println("操作之前要做.....");
        List<Employee> all = mapper.getAll();
        System.out.println("操作完成后要做.....");
        return all;
    }

    @Override
    public void insertEmployee(Employee e) {
        System.out.println("操作之前要做.....");
        mapper.insertEmployee(e);
        System.out.println("操作完成后要做.....");
    }

    @Override
    public void updateEmployee(Employee e) {
        System.out.println("操作之前要做.....");
        mapper.updateEmployee(e);
        System.out.println("操作完成后要做.....");
    }

    @Override
    public void deleteEmployeeById(Integer id) {
        System.out.println("操作之前要做.....");
        mapper.deleteEmployeeById(id);
        System.out.println("操作完成后要做.....");
    }

    @Override
    public void login() {
        //版本一 使用用户帐号和密码校对登录
    }

    @Override
    public List<Region> getAllRegion() {

        return null;
    }
}

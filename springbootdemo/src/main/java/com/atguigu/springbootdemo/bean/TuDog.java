package com.atguigu.springbootdemo.bean;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * Created by Smexy on 2023/10/24
 */
@Component("tu") //为对象起名字(id)
@Data
public class TuDog extends Dog
{
    private String name = "土狗";
}

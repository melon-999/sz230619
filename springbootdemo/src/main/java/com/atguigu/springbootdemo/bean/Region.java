package com.atguigu.springbootdemo.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Smexy on 2023/10/25
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Region
{
    private String id;
    private String regionName;
}

package com.atguigu.springbootdemo.bean;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Smexy on 2023/10/24
 */
@Data
@NoArgsConstructor
public class MyParam
{
    private String name;
    private Integer age;
}

package com.atguigu.springbootdemo.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;


@NoArgsConstructor //为类提供空参构造器
@AllArgsConstructor //为类提供全参构造器
@Data  //为为私有属性提供公共的getter,setter，重写hashcode，toString()
public class Employee
{
    private Integer id;
    private String lastName;
    private String gender;
    private String email;

    public static void main(String[] args) {
        Employee employee = new Employee();
        System.out.println(new Employee(1, "tom", "a", "b"));

    }

}

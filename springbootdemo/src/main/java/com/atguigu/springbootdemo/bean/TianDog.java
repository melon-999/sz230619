package com.atguigu.springbootdemo.bean;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * Created by Smexy on 2023/10/24
 */
@Component("tian") //为这个对象起id
@Data
public class TianDog extends Dog
{
    private String name = "舔狗";
}

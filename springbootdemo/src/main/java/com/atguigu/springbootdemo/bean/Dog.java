package com.atguigu.springbootdemo.bean;

import lombok.Data;

/**
 * Created by Smexy on 2023/10/24
 */
@Data
public class Dog
{
    private String name = "狗狗";
}

package com.atguigu.springbootdemo.bean;

import org.apache.tomcat.jni.Local;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;

/**
 * Created by Smexy on 2023/10/24
 */
@Configuration  //代表这个类是一个容器配置类，容器扫描到后会依次执行类中的所有标注了@Bean的方法
public class MyConfig
{
    //返回昨天日期
    @Bean  // 容器执行这个方法时，会自动把返回值放入容器
    public LocalDate getYesDate(){
        return LocalDate.now().minusDays(1);
    }
}

package com.atguigu.springbootdemo.mapper;

import com.atguigu.springbootdemo.bean.Employee;
import com.atguigu.springbootdemo.bean.Region;
import com.baomidou.dynamic.datasource.annotation.DS;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 @Mapper的作用:
    1.给程序员看，表明这个类是一个Mybaits的Mapper(Dao)
    2.给容器看，容器会为标注了这个注解的类，使用Mybatis提供的动态代理技术，创建一个实例
 */
@Mapper
public interface EmployeeMapper
{
    //没写，参考配置中的 primary 默认数据源
    Employee getEmployeeById(Integer id);

    List<Employee> getAll();

    //增删改可以不要返回值
    void insertEmployee(Employee e);

    void updateEmployee(Employee e);

    void deleteEmployeeById(Integer id);

    //这个方法的sql，放给定义的gmall数据源
    @DS("gmall")
    List<Region> getAllRegion();


}

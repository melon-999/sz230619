package com.atguigu.springbootdemo.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.util.concurrent.TimeUnit;

/**
 * Created by Smexy on 2023/11/3
 */
@Component
@Slf4j  //为当前类自动添加一个可以打印日志的属性，属性的名字默认为 log
public class MyTask
{
    private int i;
    /*
        在要调度的方法上添加

        调度的时机：
            方式一： 使用crontab 表达式编写
                * * *  *    *   *
               秒 分 时 日期  月  年

            方式二： 使用springboot内置的功能来调度
                fixedRate: 以固定的速率(以任务开始作为起始)调度。 如果上一个任务超时，会等待上一个任务完成后再调度。

                fixedDelay: 上一次调度完后，会等xx时间，再执行下一次
                initialDelay: 第一次调度时，可以延迟多久?
                zone: 配置时区
                timeUnit: 配置时间单位

     */
    //@Scheduled(cron = "*/5 * * * * *")
    @Scheduled(timeUnit = TimeUnit.SECONDS,zone = "Asia/Shanghai",fixedDelay = 5,initialDelay = 30)
    public void doTask() throws InterruptedException {

        log.info(i++ +"     hahaha 任务被执行了.....");
        Thread.sleep(7000);
    }
}
